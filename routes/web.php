<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
        return view('welcome');
});


$this->get('hotel', 'HotelController@index')->name('hotel');
$this->get('hotel/reservation', 'HotelController@reservation')->name('reservation');
$this->get('hotel/reservation/delete/{id}', 'HotelController@reservationDelete')->name('reservationDelete');
$this->post('hotel/reservation_room', 'HotelController@reservation_room')->name('reservation_room');
$this->post('hotel/reservation_room/save', 'HotelController@reservation_room_save')->name('reservation_room_save');
$this->get('hotel/passport_upload/{id}', 'HotelController@passport_upload')->name('passport_upload');
$this->get('hotel/reservation_list', 'HotelController@reservation_list')->name('reservation_list');
$this->get('hotel/reservation_list/hapus/{id}', 'HotelController@reservation_list_hapus')->name('reservation_list_hapus');
$this->get('hotel/check_in', 'HotelController@check_in')->name('check_in');
$this->get('hotel/check_in/add/{id}', 'HotelController@check_in_add')->name('check_in_add');
$this->get('hotel/check_out', 'HotelController@check_out')->name('check_out');
$this->get('hotel/check_out/{id}', 'HotelController@check_out_final')->name('check_out_final');
$this->get('hotel/services/', 'HotelController@services');
$this->get('hotel/services/{id}', 'HotelController@services_detail');
$this->post('hotel/services/add', 'HotelController@services_add');
$this->get('hotel/services/delete/{id}', 'HotelController@services_delete');
$this->post('hotel/services/edit/{id}', 'HotelController@services_edit');
$this->get('hotel/cetakpengunjung/{id}', 'HotelController@cetakpengunjung');
$this->get('hotel/roomtype/', 'HotelController@roomtype');
$this->post('hotel/roomtype/save', 'HotelController@addRoomtype');
$this->post('hotel/roomtype/edit', 'HotelController@editRoomtype');
$this->get('hotel/roomtype/delete/{id}', 'HotelController@deleteRoomtype');
$this->get('hotel/room/', 'HotelController@roomList');
$this->post('hotel/room/add', 'HotelController@addRoom');
$this->post('hotel/room/save', 'HotelController@editRoom');
$this->get('hotel/room/delete/{id}', 'HotelController@deleteRoom');
$this->get('hotel/room/clean/{id}', 'HotelController@cleanRoom');
$this->get('hotel/room/occupancyreport', 'HotelController@occupancyReport');
$this->post('hotel/room/occupancyreport/cetak', 'HotelController@occupancyReportpdf');

$this->get('hotel/member', 'MemberController@index');
$this->post('hotel/member/add', 'MemberController@store');
$this->get('hotel/member/delete/{id}', 'MemberController@destroy');
$this->post('hotel/member/edit', 'MemberController@update');

$this->get('group', 'GroupController@index');
$this->post('group/add', 'GroupController@add');
$this->get('group/delete/{id}', 'GroupController@delete');
$this->post('group/edit', 'GroupController@save');
$this->get('group/members/{id}', 'GroupController@members');

$this->get('restaurant', 'RestaurantController@order');
$this->get('restaurant/cashier', 'RestaurantController@cashier');
$this->post('restaurant/order_save', 'RestaurantController@order_save');
$this->get('restaurant/cashier/{id}', 'RestaurantController@payment');
$this->get('restaurant/cashier/cancel/{id}', 'RestaurantController@cancel');
$this->get('tes', 'RestaurantController@tes');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

//Inventaris routes
$this->get('inventaris', 'InventarisController@index');
$this->get('inventaris/satuan', 'InventarisController@satuan');
$this->get('inventaris/ajax/getsatuan/{id}', 'InventarisController@getsatuan');
$this->get('inventaris/ajax/hapussatuan/{id}', 'InventarisController@hapussatuan');
$this->post('inventaris/ajax/suntingsatuan/', 'InventarisController@suntingsatuan');
$this->post('inventaris/ajax/tambahsatuan/', 'InventarisController@tambahsatuan');
$this->get('inventaris/table/satuan/', 'InventarisController@tablesatuan');

$this->get('inventaris/golongan', 'InventarisController@golongan');
$this->get('inventaris/ajax/getgolongan/{id}', 'InventarisController@getgolongan');
$this->get('inventaris/ajax/hapusgolongan/{id}', 'InventarisController@hapusgolongan');
$this->post('inventaris/ajax/suntinggolongan/', 'InventarisController@suntinggolongan');
$this->post('inventaris/ajax/tambahgolongan/', 'InventarisController@tambahgolongan');
$this->get('inventaris/table/golongan/', 'InventarisController@tableGolongan');

$this->get('inventaris/data', 'InventarisController@data');
$this->get('inventaris/ajax/getData/{id}', 'InventarisController@getData');
$this->get('inventaris/ajax/hapusdata/{id}', 'InventarisController@hapusdata');
$this->post('inventaris/ajax/suntingdata/', 'InventarisController@suntingdata');
$this->post('inventaris/ajax/suntingjumlah/', 'InventarisController@suntingjumlah');
$this->post('inventaris/ajax/tambahdata/', 'InventarisController@tambahdata');
$this->get('inventaris/table/data/', 'InventarisController@tabledata');

$this->get('inventaris/laporan/basic', 'InventarisController@laporanBasic');
$this->post('inventaris/laporan/basic/cetak', 'InventarisController@cetakLaporan');
$this->get('inventaris/laporan/basic/download/{id}', 'InventarisController@downloadLaporan');
$this->get('inventaris/laporan/keluarmasuk', 'InventarisController@keluarMasuk');
$this->post('inventaris/laporan/keluarmasuk/bulanini', 'InventarisController@cetakKeluarMasukBulanIni');
$this->post('inventaris/laporan/keluarmasuk/cetak', 'InventarisController@cetakKeluarMasukPilihBulan');


$this->get('tes/', 'InventarisController@tes');

Route::get('/', 'HomeController@index')->name('home');
