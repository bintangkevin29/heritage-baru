<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;

use App\resto_menu;
use App\resto_type_menu;

class RestaurantController extends Controller
{
	function order()
	{
		$menu = DB::table('menu_list')
		->get();
		$kamar = DB::table('kamar_terisi')
		->get(); 
		return view('restaurant.order', compact('menu', 'kamar'));
	}

	function order_save(Request $request)
	{
		$method = $request->method();

		if($request->order_as == '1')
		{
			DB::table('resto_order')
			->insert(
				[
					'id_reservasi_kamar' => '0',
					'no_meja' => $request->table_num,
					'is_breakfast' => 2,

				]);
			$last_id = DB::table('resto_order')
			->orderBy('id_order', 'desc')
			->first()
			->id_order;

			
		}
		else
		{
			// echo $request->rum_num
			// DB::enableQueryLog();

			
			
			// DB::enableQueryLog();
			if($request->is_breakfast == '1')
			{	
				$sekarang = date('Y-m-d', time());

				$kamar = DB::table('kamar_redeem_breakfast')
				->where('id_reservasi_kamar','=', $request->room_num)
				->where('redeem_time' , '=' , $sekarang)
				->first();
				// ->used_coupon;
				// var_dump($kamar);exit();
			// print_r(DB::getQueryLog());
   //      exit();
				if(!($kamar))
				{
					// echo "tes";
					return redirect('restaurant/')->with('message','<div class="alert alert-danger alert-dismissable">Coupon not available</div>');
				}
				else
				{
					if($kamar->used_coupon == 1)
					{
						return redirect('restaurant/')->with('message','<div class="alert alert-danger alert-dismissable">Coupon already redeemed</div>');

					}
					else
					{
						DB::table('resto_order')
						->insert(
							[
								'id_reservasi_kamar' => $request->room_num,
								'no_meja' => $request->table_num,
								'is_breakfast' => 1,
							]);
						$sekarang = date('Y-m-d', time());
						DB::table('resto_breakfast_redeem')
						->where('id_reservasi_kamar','=',$request->room_num)
						->where('redeem_time' , '=' , $sekarang)
						->update(
							[
								'used_coupon' => '1'
							]);
						return redirect('restaurant/')->with('message','<div class="alert alert-success alert-dismissable">Successed ordering breakfast</div>');	
					}
				}

				

			}
			else
			{
				DB::table('resto_order')
				->insert(
					[
						'id_reservasi_kamar' => $request->room_num,
						'no_meja' => $request->table_num,
						'is_breakfast' => 2,

					]);

			}


				// print_r(DB::getQueryLog());
    //     exit();
			$last_id = DB::table('resto_order')
			->orderBy('id_order', 'desc')
			->first()
			->id_order;
			
			
		}
		// echo $request->pesan;exit;
		foreach($request->pesan as $pesan)
		{
			$dmenu = DB::table('resto_menu')
			->where('id_menu','=', $pesan)
			->first();
			DB::table('resto_rinc_order')
			->insert(
				[
					'id_order' => $last_id,
					'amount' => 1,
					'id_menu' => $pesan,
					'id_menu_p' => $dmenu->id_menu,
					'desc_menu_p' => $dmenu->desc_menu,
					'price_p' => $dmenu->price,
				]);
		}

		return redirect('restaurant/')->with('message','Successed');	}

		function tes()
		{
			$data = resto_menu::orderBy('id_menu_type')->get();
			return view('tes', compact('data'));
		}

		function cashier()
		{
			$data = DB::table('cashier_bills')
			->where('paid', '=', 1)
			->where('is_breakfast','=',2)
			->get();

			return view('restaurant.cashier', compact('data'));
		}

		function payment($id)
		{
			$data = DB::table('cashier_detail')
			->where('id_order','=',$id)
			->get();
			$total = DB::table('cashier_detail')
			->select(DB::raw('sum(price_p) as total'))
			->where('id_order','=',$id)
			->first()
			->total;

			return view('restaurant.payment', compact('data','total'));
		}

		function cancel($id)
		{
			DB::table('resto_rinc_order')
			->where('id_order','=',$id)
			->delete();
			DB::table('resto_order')
			->where('id_order','=',$id)
			->delete();

			return redirect('restaurant/cashier');
		}
	}
