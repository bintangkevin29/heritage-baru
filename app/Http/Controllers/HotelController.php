<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;

use Carbon\Carbon;

use PDF;

use App\Helpers;


class HotelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index()
    {
        return view('hotel.index');
    }

    function reservation()
    {
        $data = \App\CompanyModel::get();
        $kamar = \App\RoomModel::get();
        $tipe = \App\Kamar_TipeModel::get();
        return view('hotel.reservation', compact('data', 'kamar', 'tipe'));
    }

    function check_in()
    {
        DB::enableQueryLog();
        $data = \App\Reservation_ListModel::leftJoin(
            'reservasi_log',
            function ($join) {
                $join->on('reservation_list.id_reservasi', '=', 'reservasi_log.id_reservasi');
            }
        )
            ->select(DB::raw('reservasi_log.id_reservasi as id_log, reservation_list.*'))
            ->wherenull('reservasi_log.id_reservasi')
            ->orderby('id_log', 'asc')->get();
        // print_r(DB::getQueryLog());
        // exit();
        return view('hotel.check_in', compact('data'));
    }

    function check_out()
    {
        DB::enableQueryLog();
        $data = \App\Reservation_ListModel::join(
            'reservasi_log',
            function ($join) {
                $join->on('reservation_list.id_reservasi', '=', 'reservasi_log.id_reservasi');
            }
        )
            ->select(DB::raw('reservasi_log.id_reservasi as id_log, reservasi_log.check_in as ccheck_in, reservasi_log.check_out as cchek_out, reservation_list.*'))
            ->wherenull('reservasi_log.check_out')
            ->orderby('id_log', 'asc')->get();
        // print_r(DB::getQueryLog());
        // exit();
        return view('hotel.check_out', compact('data'));
    }

    function check_out_final($id)
    {
        $data = \App\Reservation_ListModel::where('id_reservasi', '=', $id)->get();
        foreach ($data as $key => $value) {
        }
        DB::table('reservasi_log')
            ->where('id_reservasi', '=', $id)
            ->update([
                'check_out' =>  DB::raw('now()'),
            ]);
        $data = DB::table('reservasi_kamar_rincian')
            ->where('id_reservasi', '=', $id)
            ->get();

        foreach ($data as $dt) {
            DB::table('kamar_rincian')
                ->where('id_kamar', '=', $dt->id_kamar)
                ->update([
                    'iscleaned' => 0,
                ]);
        }
        return redirect('hotel/check_out');
    }



    function check_in_add($id)
    {
        // $data = \App\Reservation_ListModel::orderby('id_reservasi','asc')->get();
        DB::table('reservasi_log')->insert(
            [
                'check_in' => DB::raw('now()'),
                'id_reservasi' => $id,
            ]
        );
        $selisih = DB::table('reservasi_data')
            ->where('id_reservasi', '=', $id)
            ->first();
        $reservasi_kamar = DB::table('reservasi_kamar_rincian')
            ->where('id_reservasi', '=', $id)
            ->get();
        $in = strtotime($selisih->check_in);
        $out = strtotime($selisih->check_out);
        $datediff = $out - $in;
        $gap =  round($datediff / (60 * 60 * 24));
        foreach ($reservasi_kamar as $key => $vrk) {
            $masuk = date("Y-m-d", strtotime($selisih->check_in) + 86400);
            for ($i = 1; $i <= $gap; $i++) {
                DB::table('resto_breakfast_redeem')
                    ->insert(
                        [
                            'id_reservasi_kamar' => $vrk->id_reservasi_kamar,
                            'redeem_time' => $masuk
                        ]
                    );
                $masuk = date("Y-m-d", strtotime($masuk) + 86400);
            }
        }
        return redirect('hotel/check_in');
    }

    public function reservation_room(Request $request)
    {
        $method = $request->method();
        $data = array(
            $request->title,
            $request->name,
            $request->surname,
            $request->country_arrival,
            $request->address,
            $request->postal_code,
            $request->phone,
            $request->email,
            $request->dob,
            $request->pob,
            $request->country_origin,
            $request->reservation_date,
            $request->check_in,
            $request->check_out,
            // $request->company,
            $request->deposit,
        );
        $check_in = $request->check_in;
        $check_out = $request->check_out;
        // var_dump($data);
        // exit();
        $kamar = \App\RoomModel::get();
        $tipe = \App\Kamar_TipeModel::get();

        $whereQuery = "(`tanggal` + INTERVAL 12 HOUR) >= '" . $check_in . "' + INTERVAL 13 HOUR and (`tanggal` + INTERVAL 12 HOUR) <= '" . $check_out . "' + INTERVAL 13 HOUR";
        $cek = \App\Cek_KamarModel::whereRaw($whereQuery)->get();
        return view('hotel.reservation_room', compact('data', 'kamar', 'tipe', 'check_in', 'check_out', 'cek'));
    }

    public function reservationDelete($id)
    {
        DB::table('reservasi_log')
            ->where('id_reservasi', '=', $id)
            ->delete();
        DB::table('reservasi_kamar_rincian')
            ->where('id_reservasi', '=', $id)
            ->delete();
        DB::table('reservasi_cancel_data')
            ->where('id_reservasi', '=', $id)
            ->delete();
        DB::table('reservasi_data')
            ->where('id_reservasi', '=', $id)
            ->delete();

        return redirect(route('reservation_list'));
    }

    public function roomList()
    {
        $data = Helpers::selectAll('kamar_rincian');
        $dataTipe = Helpers::selectAll('kamar_tipe');
        return view('hotel.roomlist', compact('data', 'dataTipe'));
    }

    public function addRoom(Request $request)
    {
        DB::table('kamar_rincian')
            ->insert([
                'id_tipe' => $request->roomType,
                'no_kamar' => $request->roomNumber,
                'iscleaned' => 0,
            ]);
        return redirect('hotel/room');
    }
    public function editRoom(Request $request)
    {
        // DB::enableQueryLog();
        DB::table('kamar_rincian')
            ->where('id_kamar', '=', $request->roomId)
            ->update([
                'id_tipe' => $request->roomType,
                'no_kamar' => $request->roomNumber
            ]);
        // print_r(DB::getQueryLog());
        return redirect('hotel/room');
    }

    public function cleanRoom($id)
    {
        // DB::enableQueryLog();
        DB::table('kamar_rincian')
            ->where('id_kamar', '=', $id)
            ->update([
                'iscleaned' => 1,
            ]);
        // print_r(DB::getQueryLog());
        return redirect('hotel/room');
    }
    public function roomtype()
    {
        $data = Helpers::selectAll('kamar_tipe');
        return view('hotel.rooms', compact('data'));
    }

    public function addRoomtype(Request $request)
    {
        DB::table('kamar_tipe')
            ->insert([
                'tipe' => $request->tipe,
                'harga_umum' => $request->price,
                'harga_partner' => 0,
            ]);
        return redirect('hotel/roomtype');
    }

    public function deleteRoom($id)
    {
        DB::table('kamar_rincian')
            ->where('id_kamar', '=', $id)
            ->delete();


        return redirect('hotel/room');
    }
    public function editRoomType(Request $request)
    {
        DB::enableQueryLog();
        // print_r(DB::getQueryLog());
        DB::table('kamar_tipe')
            ->where('id_tipe', '=', $request->id_tipe)
            ->update([
                'tipe' => $request->tipe,
                'harga_umum' => $request->price
            ]);
        return redirect('hotel/roomtype');
    }
    public function deleteRoomtype($id)
    {
        DB::table('kamar_rincian')
            ->where('id_tipe', '=', $id)
            ->delete();

        DB::table('kamar_tipe')
            ->where('id_tipe', '=', $id)
            ->delete();


        return redirect('hotel/roomtype');
    }


    public function occupancyReport()
    {
        $tanggal = Carbon::now();
        $roomType = "";
        $tanggalKirim = Carbon::now()->toFormattedDateString();
        $bulan = $tanggal->month;
        $week = $tanggal->subdays(10);
        $data = DB::table('kamar_log_status')
            ->where('tanggal', '>=', $week)
            ->orderBy('tanggal', 'ASC')
            ->get();
        $tipe = "";
        $kamar = Helpers::selectAll('kamar_rincian');
        $tipe_kamar = Helpers::selectAll('kamar_tipe');

        // return view('pdf.occupancyReport', compact('data', 'tanggalKirim', 'kamar', 'roomType', 'tipe'));
        $pdf = PDF::loadview('pdf.occupancyReport', ['data' => $data, 'tanggalKirim' => $tanggalKirim, 'kamar' => $kamar, 'roomType' => $roomType, 'tipe' => $tipe])->setPaper('a4')->setOrientation('landscape');
        return view('hotel.reportOccupancy', compact('data', 'tanggalKirim', 'kamar', 'tipe_kamar', 'tipe', 'roomType'));
    }

    public function occupancyReportpdf(Request $request)
    {
        $tanggal = Carbon::parse($request->bulan);
        $roomType = $request->roomType;
        $tanggalKirim = $tanggal->endOfMonth()->toFormattedDateString();
        $bulan = $tanggal->month;
        $tahun = $tanggal->year;
        // $bulanApa = $bulan->toFormattedDateString();
        // $tipe="";

        if ($roomType == "all") {
            $data = DB::table('kamar_log_status')
                ->whereRaw('MONTH(tanggal) = ' . $bulan)
                ->whereRaw('YEAR(tanggal) = ' . $tahun)
                ->orderBy('tanggal', 'ASC')
                ->get();

            $tipe = "";

            $kamar = Helpers::selectAll('kamar_rincian');
        }

        // ->select('reservasi_data.*','no_kamar','tipe','harga_umum')
        // ->leftJoin('reservasi_kamar_rincian','reservasi_data.id_reservasi','=','reservasi_kamar_rincian.id_reservasi')
        // ->leftJoin('kamar_rincian','reservasi_kamar_rincian.id_kamar','=', 'kamar_rincian.id_kamar')
        // ->leftJoin('kamar_tipe','kamar_tipe.id_tipe','=','kamar_tipe.id_tipe')
        // ->where('reservasi_data.id_reservasi', '=', $id)
        // ->first();
        else {
            $data = DB::table('kamar_log_status')
                ->SELECT('kamar_log_status.*', 'id_tipe')
                ->leftJoin('kamar_rincian', 'kamar_log_status.id_kamar', '=', 'kamar_rincian.id_kamar')
                ->where('id_tipe', '=', $roomType)
                ->whereRaw('MONTH(tanggal) = ' . $bulan)
                ->whereRaw('YEAR(tanggal) = ' . $tahun)
                ->orderBy('tanggal', 'ASC')
                ->get();

            $kamar = DB::table('kamar_rincian')
                ->SELECT('*')
                ->where('id_tipe', '=', $roomType)
                ->GET();


            $tipe = DB::table('kamar_tipe')
                ->SELECT('tipe')
                ->where('id_tipe', '=', $roomType)
                ->GET();
        }
        if ($request->excel) {
            $excel = $request->excel;
            return view('pdf.occupancyReport', compact('data', 'tanggalKirim', 'kamar', 'roomType', 'tipe', 'bulan', 'excel'));
        }

        $pdf = PDF::loadview('pdf.occupancyReport', ['data' => $data, 'tanggalKirim' => $tanggalKirim, 'kamar' => $kamar, 'bulan' => $bulan, 'tipe' => $tipe, 'roomType' => $roomType])->setPaper('a4')->setOrientation('landscape');
        return $pdf->download('occupancy_report' . $bulan . '.pdf');
        // return view('hotel.reportOccupancy', compact('data', 'tanggalKirim', 'kamar', 'tipe'));
        // \\return view('hotel.reportOccupancy', compact('data', 'tanggalKirim', 'kamar'));
    }

    function reservation_room_save(Request $request)
    {


        // $request->name, f2
        // $request->surname, f3
        // $request->country_arrival, f4
        // $request->address, f5
        // $request->postal_code, f6
        // $request->phone, f7
        // $request->email, 8 
        // $request->dob,9
        // $request->pob, 10
        // $request->country_origin, f11
        // $request->reservation_date, 12
        // $request->check_in, 13
        // $request->check_out, 14
        // // $request->company, 15
        // $request->deposit, 16

        DB::table('guest_data')->insert(
            [
                'title' => $request->f1,
                'first_name' => $request->f2,
                'surname' => $request->f3,
                'address' => $request->f5,
                'postal_code' => $request->f6,
                'country' => $request->f4,
                'telephone' => $request->f7,
                'email' => $request->f8,
                'dob' => $request->f9,
                'pob' => $request->f10,
                'nationality' => $request->f11,
            ]
        );
        if ($request->f15 == '') {
            $company = '0';
        } else {
            $company = '1';
        }
        $id_guest = \App\Guest_DataModel::orderBy('id_guest', 'desc')->limit(1)->get();
        foreach ($id_guest as $key => $value1) {
        }
        DB::table('reservasi_data')->insert(
            [
                'id_guest' => $value1->id_guest,
                'waktu_reservasi' => $request->f12,
                'check_in' => $request->f13,
                'check_out' => $request->f14,
                'guest_tipe' => $company,
                'deposit' => $request->f16,
                'company' => $request->f15,
            ]
        );
        $id = \App\Reservasi_DataModel::orderBy('id_reservasi', 'desc')->limit(1)->get();
        foreach ($id as $key => $value) {
        }

        if ($request->file != '') {

            $user = Auth::user();
            $file = $request->file('file');
            $tujuan_upload = base_path() .  '/assets/img/passport';
            $nama = time() . '_' . $file->getClientOriginalName();
            echo $namafile = $tujuan_upload . '/' . $nama;
            Image::make(Input::file('file'))->encode('jpg', 75)->save($namafile);
        }

        // DB::table('guest_data')->insert(
        //     [
        //         'img_passport' => $nama,
        //     ]
        // );

        DB::table('guest_data')
            ->where('id_guest', '=', $value1->id_guest)
            ->update(
                [
                    'img_passport' => $nama,
                ]
            );


        $in = strtotime($request->f13);
        $out = strtotime($request->f14);
        $datediff = $out - $in;
        $selisih =  round($datediff / (60 * 60 * 24));

        echo $selisih;

        foreach ($request->kamar as $data) {
            DB::table('reservasi_kamar_rincian')->insert(
                [
                    'id_reservasi' => $value->id_reservasi,
                    'id_kamar' => $data,
                ]
            );


            DB::table('kamar_log_status')->insert(
                [
                    'id_kamar' => $data,
                    'status' => '0',
                    'tanggal' => $request->f13,
                    'id_reservasi' => $value->id_reservasi,
                ]
            );
            for ($i = 1; $i <= $selisih; $i++) {
                DB::table('kamar_log_status')->insert(
                    [
                        'id_kamar' => $data,
                        'status' => '0',
                        'tanggal' => DB::raw('DATE_SUB("' . $request->f13 . '", INTERVAL -' . $i . ' DAY)'),
                        'id_reservasi' => $value->id_reservasi,
                    ]
                );
            }
        }


        return redirect('hotel/reservation_list');
    }

    function reservation_list()
    {
        $data = \App\Reservation_ListModel::orderby('id_reservasi', 'desc')->get();
        return view('hotel.reservation_list', compact('data'));
    }

    function reservation_list_hapus($id)
    {
        // DB::table('reservasi_data')
        // ->where('id_reservasi', '=', $id)
        // ->delete();

        DB::table('reservasi_kamar_rincian')
            ->where('id_reservasi', '=', $id)
            ->delete();

        DB::table('kamar_log_status')
            ->where('id_reservasi', '=', $id)
            ->delete();

        DB::table('reservasi_cancel_data')->insert(
            [
                'waktu' => DB::raw('now()'),
                'id_reservasi' => $id,
            ]
        );

        $data = \App\Reservation_ListModel::orderby('id_reservasi', 'asc')->get();
        return redirect('hotel/reservation_list');
    }

    public function cetakpengunjung($id)
    {
        // $data = DB::table('reservasi_data')
        //     ->select('*')
        //     ->where('id_reservasi', '=', $id)
        //     ->first();

        $data = DB::table('reservasi_data')
            ->select('reservasi_data.*', 'no_kamar', 'tipe', 'harga_umum')
            ->leftJoin('reservasi_kamar_rincian', 'reservasi_data.id_reservasi', '=', 'reservasi_kamar_rincian.id_reservasi')
            ->leftJoin('kamar_rincian', 'reservasi_kamar_rincian.id_kamar', '=', 'kamar_rincian.id_kamar')
            ->leftJoin('kamar_tipe', 'kamar_tipe.id_tipe', '=', 'kamar_tipe.id_tipe')
            ->where('reservasi_data.id_reservasi', '=', $id)
            ->first();

        $dataGuest = DB::table('guest_data')
            ->select('*')
            ->where('id_guest', '=', $data->id_guest)
            ->first();

        $reservationID = $id;

        $tanggalReservasi = Carbon::parse($data->waktu_reservasi)->toFormattedDateString();

        // return view('pdf.cetakguest', compact('dataGuest', 'reservationID', 'data','tanggalReservasi'));
        $pdf = PDF::loadview('pdf.cetakguest', ['dataGuest' => $dataGuest, 'reservationID' => $reservationID, 'data' => $data, 'tanggalReservasi' => $tanggalReservasi]);
        return $pdf->download('guest_' . $id . '.pdf');
    }


    function passport_upload_save(Request $request)
    {
        // $user = Auth::user();
        // $file = $request->file('file');
        // $tujuan_upload = '../assets/img/blog/';
        // $nama = time().'_'.$file->getClientOriginalName();
        // $namafile = $tujuan_upload.'/'.$nama;
        // $namafile_thumb = $tujuan_upload.'/thumb_'.$nama;
        // $namafile_large = $tujuan_upload.'/large_'.$nama;
        // Image::make(Input::file('file'))->fit(754, 462)->save($namafile);
        // Image::make(Input::file('file'))->fit(1161, 590)->save($namafile_large);
        // Image::make(Input::file('file'))->fit(100, 61)->save($namafile_thumb);




        DB::table('guest_data')->insert(
            [
                'title' => $request->f1,
                'first_name' => $request->f2,
                'surname' => $request->f3,
                'address' => $request->f5,
                'country' => $request->f4,
                'telephone' => $request->f6,
                'email' => $request->f7,
                'dob' => $request->f8,
                'pob' => $request->f9,
                'nationality' => $request->f10,
            ]
        );
        if ($request->f14 == '') {
            $company = '0';
        } else {
            $company = '1';
        }
        $id_guest = \App\Guest_DataModel::orderBy('id_guest', 'desc')->limit(1)->get();
        foreach ($id_guest as $key => $value1) {
        }
        DB::table('reservasi_data')->insert(
            [
                'id_guest' => $value1->id_guest,
                'waktu_reservasi' => $request->f11,
                'check_in' => $request->f12,
                'check_out' => $request->f13,
                'guest_tipe' => $company,
                'deposit' => $request->f15,
                'company' => $request->f14,
            ]
        );
        $id = \App\Reservasi_DataModel::orderBy('id_reservasi', 'desc')->limit(1)->get();
        foreach ($id as $key => $value) {
        }

        $in = strtotime($request->f12);
        $out = strtotime($request->f13);
        $datediff = $out - $in;
        $selisih =  round($datediff / (60 * 60 * 24));

        foreach ($request->kamar as $data) {
            DB::table('reservasi_kamar_rincian')->insert(
                [
                    'id_reservasi' => $value->id_reservasi,
                    'id_kamar' => $data,
                ]
            );


            DB::table('kamar_log_status')->insert(
                [
                    'id_kamar' => $data,
                    'status' => '0',
                    'tanggal' => $request->f12,
                ]
            );
            for ($i = 1; $i <= $selisih; $i++) {
                DB::table('kamar_log_status')->insert(
                    [
                        'id_kamar' => $data,
                        'status' => '0',
                        'tanggal' => DB::raw('DATE_SUB("' . $request->f12 . '", INTERVAL -' . $i . ' DAY)'),
                    ]
                );
            }
        }


        return redirect('hotel/passport_upload/' . $value->id_reservasi);
    }

    function services()
    {
        DB::enableQueryLog();
        $data = \App\Reservation_ListModel::join(
            'reservasi_log',
            function ($join) {
                $join->on('reservation_list.id_reservasi', '=', 'reservasi_log.id_reservasi');
            }
        )
            ->select(DB::raw('reservasi_log.id_reservasi as id_log, reservasi_log.check_in as ccheck_in, reservasi_log.check_out as cchek_out, reservation_list.*'))
            ->wherenull('reservasi_log.check_out')
            ->orderby('id_log', 'asc')->get();
        // print_r(DB::getQueryLog());
        // exit();
        return view('hotel.services', compact('data'));
    }

    function services_detail($id)
    {
        DB::enableQueryLog();
        $data = \App\Reservation_ListModel::where('id_reservasi', '=', $id)->get();
        $services = DB::table('services_guest')
            ->where('id_reservasi', '=', $id)
            ->get();
        $basics = DB::table('data_standard_services')
            ->get();
        // print_r(DB::getQueryLog());
        // exit();
        return view('hotel.services_detail', compact('data', 'services', 'basics'));
    }

    function services_add(Request $request)
    {
        $method = $request->method();
        DB::table('data_payment_service')
            ->insert(
                [
                    'id_reservasi' => $request->id_reservasi,
                    'id_services' => $request->services,
                    'service_price' => $request->price,
                    'tanggal' => DB::raw('now()'),
                ]
            );
        return redirect('hotel/services/' . $request->id_reservasi);
    }

    function services_delete($id)
    {
        // $method = $request->method();
        $data = DB::table('data_payment_service')
            ->where('id_payment', '=', $id)
            ->get();
        DB::table('data_payment_service')
            ->where('id_payment', '=', $id)
            ->delete();

        foreach ($data as $key => $value) {
            # code...
        }
        return redirect('hotel/services/' . $value->id_reservasi);
    }

    function services_edit(Request $request)
    {
        $method = $request->method();
        DB::table('data_payment_service')
            ->where('id_payment', '=', $request->id_payment)
            ->update(
                [
                    'service_price' => $request->price,
                ]
            );

        return redirect('hotel/services/' . $request->id_reservasi);
    }
}
