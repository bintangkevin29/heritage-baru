<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers;
use Illuminate\Support\Facades\DB;


class GroupController extends Controller
{
    public function index()
    {
        $data = Helpers::selectAll('hotel_group');
        return view('hotel.group', compact('data'));
    }

    public function add(Request $request)
    {
        DB::table('hotel_group')
            ->insert([
                'nama' => $request->name,
                'alamat' => $request->address,
                'keterangan' => $request->details
            ]);

        return redirect(url('group'))->with(['success' => 'Successfully added']);
    }

    public function delete($id)
    {
        DB::table('hotel_group')
            ->where('id', '=', $id)
            ->delete();

        return redirect(url('group'))->with(['success' => 'Successfully deleted']);
    }

    public function save(Request $request)
    {
        DB::table('hotel_group')
            ->where('id', '=', $request->id)
            ->update([
                'nama' => $request->name,
                'alamat' => $request->address,
                'keterangan' => $request->details
            ]);
        return redirect(url('group'))->with(['success' => 'Successfully edited']);
    }

    public static function getGroupName($id)
    {
        $data = DB::table('hotel_group')
            ->where('id', '=', $id)
            ->first();

        return $data->nama;
    }

    public function members($id)
    {
        $data = DB::table('hotel_member')
            ->where('id_group', '=', $id)
            ->get();

        $data_group = DB::table('hotel_group')
            ->where('id', '=', $id)
            ->first();

        return view('hotel.group_member', compact('data', 'data_group'));
    }

    public static function membersCount($id)
    {
        $data = DB::table('hotel_member')
            ->where('id_group', '=', $id)
            ->count();

        return $data;
    }
}
