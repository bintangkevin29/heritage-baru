<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

use PDF;


class InventarisController extends Controller
{

    public function __construct()
    {
        $modul = 'Inventaris';

        View::share('modul', $modul);
    }
    public function index()
    {
        return view('inventaris.index');
    }

    function data()
    {
        $dataSatuan = DB::table('inventaris_satuan')
            ->select('*')
            ->get();
        $dataGolongan = DB::table('inventaris_golongan')
            ->select('*')
            ->get();
        $title = 'Data Inventaris';
        return view('inventaris.data', compact('title', 'dataSatuan', 'dataGolongan'));
    }

    function getdata($id)
    {
        $dataSatuan = DB::table('inventaris_satuan')
            ->select('*')
            ->get();
        $dataGolongan = DB::table('inventaris_golongan')
            ->select('*')
            ->get();
        DB::enableQueryLog();
        $data = DB::table('inventaris_data')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
        // print_r(DB::getQueryLog());
        // exit();
        foreach ($data as $dt) {
            $id = $dt->id;
            $namaBarang = $dt->nama_barang;
            $id_golongan = $dt->id_golongan;
            $id_satuan = $dt->id_satuan;
            $jumlah = $dt->jumlah;
        }
        return view('ajax.inventaris.getData', compact('data', 'id', 'namaBarang', 'id_golongan', 'id_satuan', 'jumlah', 'dataSatuan', 'dataGolongan'));
    }

    function tabledata()
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_data')
            ->leftJoin('inventaris_golongan', 'inventaris_data.id_golongan', '=', 'inventaris_golongan.id')
            ->leftJoin('inventaris_satuan', 'inventaris_data.id_satuan', '=', 'inventaris_satuan.id')
            ->select(DB::raw('*, inventaris_data.id as id_data,inventaris_golongan.id as id_golongan'))
            ->get();
        // print_r(DB::getQueryLog());
        // exit();  
        return view('inventaris.table.data', compact('data'));
    }

    function tambahdata(Request $request)
    {
        $check = DB::table('inventaris_data')->where('nama_barang', '=', $request->namaBarang)->get();
        if (count($check) > 0) {
            return response()->json('Nama sudah ada', 500);
        }

        DB::table('inventaris_data')
            ->insert(
                [
                    'nama_barang' => $request->namaBarang,
                    'jumlah' => $request->jumlah,
                    'jumlah_awal' => $request->jumlah,
                    'id_satuan' => $request->satuan,
                    'id_golongan' => $request->golongan,
                ]
            );

        return 'yey';
    }

    function hapusdata($id)
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_data')
            ->where('id', '=', $id)
            ->delete();
        return 'Berhasil';
    }

    public function getSatuanName($id)
    {
        $data = DB::table('inventaris_satuan')
            ->where('id', '=', $id)
            ->get();

        foreach ($data as $dt) {
        }
        return $dt->satuan;
    }

    public function getDataName($id)
    {
        $data = DB::table('inventaris_data')
            ->where('id', '=', $id)
            ->get();

        foreach ($data as $dt) {
        }
        return $dt;
    }

    public function getGolonganName($id)
    {
        $data = DB::table('inventaris_golongan')
            ->where('id', '=', $id)
            ->get();

        foreach ($data as $dt) {
        }
        return $dt->golongan;
    }

    function suntingdata(Request $request)
    {
        // DB::enableQueryLog();

        DB::table('inventaris_data')
            ->where('id', '=', $request->id)
            ->update(
                [
                    'nama_barang' => $request->namaBarang,
                    'id_golongan' => $request->golongan,
                    'id_satuan' => $request->satuan,
                    'jumlah' => $request->jumlah,
                ]
            );
        // $tes = DB::getQueryLog();
        return '
        <td tabindex="0" class="hasil_update sorting_1">' . $request->golongan . '.' . $request->id . '</td>
        <td class="hasil_update">' . $this->getGolonganName($request->golongan) . '</td>
        <td class="hasil_update" id="' . $request->id . '">' . $request->namaBarang . '</td>
        <td class="hasil_update">
    <span orinum="' . $request->jumlah . '" class="jumlah_' . $request->id . '">
        ' . $request->jumlah . '
    </span>
    <a value="' . $request->id . '" class="btn" id="submitValue">
        <i class="icon icon-pencil"></i>
    </a>
    <span id="showSubmit_' . $request->id . '">
        
    </span>
    <span id="showCancel_' . $request->id . '">
        
    </span></td>
        <td class="hasil_update">' . $this->getSatuanName($request->satuan) . '</td>
        <td class="hasil_update">
            <a data-toggle="modal" value="' . $request->id . '" id="bukaModal" class="btn btn-info"><i class="icon-pencil"></i> Sunting</a>
            <a value="' . $request->id . '" id="hapusdata" class="btn btn-danger"><i class="icon-trash"></i> Hapus</a>
        </td>';
    }

    function suntingjumlah(Request $request)
    {
        DB::table('inventaris_data')
            ->where('id', '=', $request->id)
            ->update(
                [
                    'jumlah' => $request->jumlah,
                ]
            );
        $jumlahAwal = $request->jumlahAwal;
        $jumlahAkhir = $request->jumlah;
        $jumlah = 0;
        $keluarmasuk = 0;
        if ($jumlahAwal <= $jumlahAkhir) {
            $jumlah = $jumlahAkhir - $jumlahAwal;
            $keluarmasuk = 2;
        } else if ($jumlahAkhir <= $jumlahAwal) {
            $jumlah = $jumlahAwal - $jumlahAkhir;
            $keluarmasuk = 1;
        }

        $namaBarang = $this->getDataName($request->id);
        $satuan = $this->getSatuanFrombarang($namaBarang->id_satuan);
        $golongan = $this->getGolonganFromBarang($namaBarang->id_golongan);

        DB::table('inventaris_riwayat')
            ->insert([
                'tanggal' => Carbon::now(),
                'keluarmasuk' => $keluarmasuk,
                'jumlah' => $jumlah,
                'jumlahAwal' => $jumlahAwal,
                'jumlahAkhir' => $jumlahAkhir,
                'pj' => $request->penanggungjawab,
                'id_data' => $request->id,
                'nama_barang' => $namaBarang->nama_barang,
                'id_satuan' => $namaBarang->id_satuan,
                'id_golongan' => $namaBarang->id_golongan,
                'golongan' => $golongan,
                'satuan' => $satuan,
            ]);
        return '
        <span orinum="' . $request->jumlah . '" class="hasil_update jumlah_' . $request->id . '">
            ' . $request->jumlah . '
        </span>
        <a value="' . $request->id . '" class="btn" id="submitValue">
            <i class="icon icon-pencil"></i>
        </a>
        <span id="showSubmit_' . $request->id . '">
            
        </span>
        <span id="showCancel_' . $request->id . '">
            
        </span>
        ';
    }



    function getsatuan($id)
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_satuan')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
        // print_r(DB::getQueryLog());
        // exit();
        foreach ($data as $dt) {
            $id = $dt->id;
            $satuan = $dt->satuan;
        }
        $title = 'satuan Barang';
        return view('ajax.inventaris.getSatuan', compact('data', 'title', 'id', 'satuan'));
    }

    function satuan()
    {

        $title = 'Satuan Barang';
        return view('inventaris.satuan', compact('title'));
    }

    function tablesatuan()
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_satuan')
            ->select('*')
            ->get();
        // print_r(DB::getQueryLog());
        // exit();
        return view('inventaris.table.satuan', compact('data'));
    }


    function hapussatuan($id)
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_satuan')
            ->where('id', '=', $id)
            ->delete();
        return 'Berhasil';
    }

    function suntingsatuan(Request $request)
    {
        DB::table('inventaris_satuan')
            ->where('id', '=', $request->id)
            ->update(
                [
                    'satuan' => $request->satuan,
                ]
            );

        return '<span class="hasil_update">' . $request->satuan . '</span>';
    }

    function tambahsatuan(Request $request)
    {
        DB::table('inventaris_satuan')
            ->insert(
                [
                    'satuan' => $request->satuan,
                ]
            );
        DB::enableQueryLog();
        $data = DB::table('inventaris_satuan')
            ->orderBy('id', 'DESC')
            ->take(1)
            ->get();
        // print_r(DB::getQueryLog());

        // exit;
        foreach ($data as $dt) {
        }
        $id = $dt->id;
        $satuan = $dt->satuan;

        return view('ajax.inventaris.addsatuan', compact('id', 'satuan'));
    }

    function tes()
    {
        return view('tes');
    }

    function golongan()
    {

        $title = 'Golongan Barang';
        return view('inventaris.golongan', compact('title'));
    }

    function tableGolongan()
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_golongan')
            ->select('*')
            ->get();
        // print_r(DB::getQueryLog());
        // exit();
        return view('inventaris.table.golongan', compact('data'));
    }

    function getgolongan($id)
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_golongan')
            ->select('*')
            ->where('id', '=', $id)
            ->get();
        // print_r(DB::getQueryLog());
        // exit();
        foreach ($data as $dt) {
            $id = $dt->id;
            $golongan = $dt->golongan;
        }
        $title = 'Golongan Barang';
        return view('ajax.inventaris.getgolongan', compact('data', 'title', 'id', 'golongan'));
    }

    function hapusgolongan($id)
    {
        DB::enableQueryLog();
        $data = DB::table('inventaris_golongan')
            ->where('id', '=', $id)
            ->delete();
        return 'Berhasil';
    }
    function suntinggolongan(Request $request)
    {
        DB::table('inventaris_golongan')
            ->where('id', '=', $request->id)
            ->update(
                [
                    'golongan' => $request->golongan,
                ]
            );

        return '<span class="hasil_update">' . $request->golongan . '</span>';
    }

    function tambahgolongan(Request $request)
    {
        DB::table('inventaris_golongan')
            ->insert(
                [
                    'golongan' => $request->golongan,
                ]
            );
        DB::enableQueryLog();
        $data = DB::table('inventaris_golongan')
            ->orderBy('id', 'DESC')
            ->take(1)
            ->get();
        // print_r(DB::getQueryLog());

        // exit;
        foreach ($data as $dt) {
        }
        $id = $dt->id;
        $golongan = $dt->golongan;

        return view('ajax.inventaris.addGolongan', compact('id', 'golongan'));
    }

    function laporanBasic()
    {
        $data = DB::table('inventaris_laporan')
            ->select('*')
            ->orderBy('id', 'DESC')
            ->get();
        $dataGolongan = DB::table('inventaris_golongan')
            ->select('*')
            ->get();
        $title = 'Laporan Inventaris';
        return view('inventaris.laporanBasic', compact('dataGolongan', 'data', 'title'));
    }

    function keluarMasuk()
    {
        $data = DB::table('inventaris_riwayat')
            ->select('*')
            ->orderBy('id', 'DESC')
            ->get();
        $title = 'Laporan Keluar &ndash; Masuk Inventaris';

        $dataGolongan = DB::table('inventaris_golongan')
            ->select('*')
            ->get();
        return view('inventaris.laporanKeluarMasuk', compact('data', 'title', 'dataGolongan'));
    }

    public function downloadLaporan($id)
    {
        $dataLaporan = DB::table('inventaris_laporan')
            ->where('id', '=', $id)
            ->first();

        $tanggal = $dataLaporan->tanggal;

        $dataDetailLaporan = DB::table('inventaris_laporan_detail')
            ->where('id_laporan', '=', $id)
            ->get();

        $pdf = PDF::loadview('pdf.laporanDownloadPDF', ['data' => $dataDetailLaporan, 'tanggal' => $tanggal]);
        // return 'ngeng';
        return $this->prosesDownloadCetakLaporan($pdf);
    }

    public static function getDetailKeluarMasuk($tanggal, $golongan)
    {
        $date = Carbon::parse($tanggal);
        $date_d = $date->format('d');
        $date_m = $date->format('m');
        $date_y = $date->format('Y');

        if ($golongan == "all") {
            $data = DB::table('inventaris_riwayat')
                ->select('*')
                ->whereRaw(
                    'MONTH(tanggal) = ' . $date_m . ' and DAY(tanggal) = ' . $date_d . ' and YEAR(tanggal) = ' .
                        $date_y
                )
                ->orderBy('tanggal', 'ASC')
                ->get();
        } else {
            $data = DB::table('inventaris_riwayat')
                ->select('*')
                ->where('id_golongan', '=', $golongan)
                ->whereRaw(
                    'MONTH(tanggal) = ' . $date_m . ' and DAY(tanggal) = ' . $date_d . ' and YEAR(tanggal) = ' .
                        $date_y
                )

                ->orderBy('tanggal', 'ASC')
                ->get();
        }

        // old query, before filter golongn ---------------
        // $data = DB::table('inventaris_riwayat')
        //     ->select('*')

        //     ->orderBy('tanggal', 'ASC')
        //     ->get();

        return $data;
    }

    public function cetakKeluarMasukBulanIni(Request $request)
    {
        $tanggal = Carbon::now();

        $date_m = $tanggal->format('m');
        $golongan = $request->golongan;

        if ($golongan == "all") {
            $data = DB::table('inventaris_riwayat')
                ->select('*')
                ->whereRaw(
                    'MONTH(tanggal) = ' . $date_m
                )
                ->orderBy('tanggal', 'ASC')
                ->get();

            $nm_golongan = "";
        } else {
            $data = DB::table('inventaris_riwayat')
                ->select('*')
                ->where('id_golongan', '=', $golongan)
                ->whereRaw(
                    'MONTH(tanggal) = ' . $date_m
                )

                ->orderBy('tanggal', 'ASC')
                // ->sharedLock()
                ->get();

            $nm_golongan = DB::table('inventaris_golongan')
                ->select('*')
                ->where('id', '=', $golongan)
                ->get();
        }

        // echo $golongan;
        $pdf = PDF::loadview('pdf.laporanKeluarMasukBulanIni', ['data' => $data, 'tanggal' => $tanggal, 'golongan' => $golongan, 'nm_golongan' => $nm_golongan]);
        return $pdf->download('laporan-keluar-masuk' . $tanggal . '.pdf');

        // return view('pdf.laporanKeluarMasukBulanIni', compact('data', 'tanggal'));
    }

    public function cetakKeluarMasukPilihBulan(Request $request)
    {
        $tanggal = Carbon::parse($request->bulan);
        $date_m = $tanggal->format('m');

        $golongan = $request->golongan;

        if ($golongan == "all") {
            $data = DB::table('inventaris_riwayat')
                ->select('*')
                ->whereRaw(
                    'MONTH(tanggal) = ' . $date_m
                )
                ->orderBy('tanggal', 'ASC')
                ->get();

            $nm_golongan = "";
        } else {
            $data = DB::table('inventaris_riwayat')
                ->select('*')
                ->where('id_golongan', '=', $golongan)
                ->whereRaw(
                    'MONTH(tanggal) = ' . $date_m
                )

                ->orderBy('tanggal', 'ASC')
                // ->sharedLock()
                ->get();


            $nm_golongan = DB::table('inventaris_golongan')
                ->select('*')
                ->where('id', '=', $golongan)
                ->get();
        }

        $tanggal = Carbon::now();

        $pdf = PDF::loadview('pdf.laporanKeluarMasukBulanIni', ['data' => $data, 'tanggal' => $tanggal, 'golongan' => $golongan, 'nm_golongan' => $nm_golongan]);

        return $pdf->download('laporan-keluar-masuk' . $tanggal . '.pdf');

        // return view('pdf.laporanKeluarMasukBulanIni', compact('data', 'tanggal'));
    }

    public function cetakLaporan(Request $request)
    {
        $golongan = $request->golongan;
        $tanggal = Carbon::now();
        DB::table('inventaris_laporan')
            ->insert(
                [
                    'tanggal' => $tanggal,
                ]
            );

        $dtl = DB::table('inventaris_laporan')
            ->select('*')
            ->orderBy('id', 'DESC')
            ->first();

        return $this->prosesCetakLaporan($dtl->id, $tanggal, $golongan, $request->excel);
    }

    public function prosesCetakLaporan($id_laporan, $tanggal, $golongan, $excel)
    {
        if ($golongan == 'all') {
            $data = DB::table('inventaris_data')
                ->select('*')
                ->orderBy('id_golongan', 'ASC')
                ->get();
            $golonganPrint = '[SEMUA]';
        } else {
            $data = DB::table('inventaris_data')
                ->select('*')
                ->where('id_golongan', '=', $golongan)
                ->orderBy('id_golongan', 'ASC')
                ->get();
            $golonganPrint = $this->getGolonganFromBarang($golongan);
        }


        foreach ($data as $dt) {
            DB::table('inventaris_laporan_detail')
                ->insert([
                    'id_laporan' => $id_laporan,
                    'id_golongan' => $dt->id_golongan,
                    'id_barang' => $dt->id,
                    'id_satuan' => $dt->id_satuan,
                    'nama_barang' => $dt->nama_barang,
                    'golongan' => $this->getGolonganFromBarang($dt->id_golongan),
                    'satuan' => $this->getSatuanFrombarang($dt->id_satuan),
                    'jumlah' => $dt->jumlah,
                ]);
        } 
        // return view('pdf.laporanPDF', compact('data', 'tanggal','golonganPrint'));
        // return $data->nama-barang;
        // return 'ngeng';
        // return view('pdf.laporanPDF', compact('data', 'tanggal', 'golonganPrint'));
        // return $this->laporanBasic();
        if ($excel) {
            return view('pdf.laporanPDF', compact('data', 'tanggal', 'golonganPrint', 'excel'));
        }
        $pdf = PDF::loadview('pdf.laporanPDF', ['data' => $data, 'tanggal' => $tanggal, 'golonganPrint' => $golonganPrint]);
        return $this->prosesDownloadCetakLaporan($pdf);
    }

    public function prosesDownloadCetakLaporan($pdf)
    {
        return $pdf->download('laporan-barang.pdf');
    }

    public static function getGolonganFromBarang($id)
    {
        $data = DB::table('inventaris_golongan')
            ->select('*')
            ->where('id', '=', $id)
            ->first();



        return $data->golongan;
    }

    public static function getSatuanFrombarang($id)
    {
        $data = DB::table('inventaris_satuan')
            ->select('*')
            ->where('id', '=', $id)
            ->first();

        return $data->satuan ?? '';
    }
}
