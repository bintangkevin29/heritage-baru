<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class resto_menu extends Model
{    
	protected $table="resto_menu";

	public function resto_type_menu()
	{
		return $this->belongsTo('App\resto_type_menu','id_menu_type');
	}
}
