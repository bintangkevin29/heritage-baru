<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class resto_type_menu extends Model
{
    //
    protected $table = 'resto_type_menu';

    public function resto_menu()
    {
    	return $this->hasMany('App\resto_menu','id_menu_type');
    }
}
