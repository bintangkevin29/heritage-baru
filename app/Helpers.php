<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class Helpers extends Model
{
    public static function selectAll($table)
    {
        $data = DB::table($table)
            ->select('*')
            ->get();

        return $data;
    }

    public static function getRoomTypeFromRoomDetail($id_kamar)
    {
        $data = DB::table('kamar_tipe')
            ->select('*')
            ->where('id_tipe', '=', $id_kamar)
            ->first();

        $tipe = $data->tipe;
        return $tipe;
    }

    public static function getDateFromRoom($day, $month, $id_kamar)
    {
        $data = DB::table('kamar_log_status')
            ->select('*')
            ->whereRaw('MONTH(tanggal) = ' . $month . ' AND DAY(tanggal) = ' . $day . ' AND id_kamar = ' . $id_kamar)
            ->first();

        if ($data == null) 
        {
            return 0;
        } 
        else {
            return 1;
        }
    }

    public static function countRoomOccupied($month, $day, $tipe)
    {
        $id_tipe = $tipe;
        if($id_tipe == 'all')
        {
            $data = $id_tipe;
            $data = DB::table('kamar_log_status') 
            ->WHERERAW('MONTH(tanggal) = ' . $month .' AND DAY(tanggal) = ' . $day)
            ->count();
        }
        
        else
        {
            $data = "error";
            $data = DB::table('kamar_log_status') 
            ->SELECT('kamar_log_status.*', 'id_tipe')
            ->leftJoin('kamar_rincian','kamar_log_status.id_kamar', '=', 'kamar_rincian.id_kamar')
            ->where('id_tipe', '=', $id_tipe)
            ->WHERERAW('MONTH(tanggal) = ' . $month .' AND DAY(tanggal) = ' . $day)
            ->count();
        }

        return $data ;
    }
}
