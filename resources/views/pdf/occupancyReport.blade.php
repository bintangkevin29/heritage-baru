<!doctype html>
<html lang="en">
<?php

use App\Http\Controllers\InventarisController;
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
        body {
            font-size: 12px;
        }

        table {
            font-size: 10px;
        }
    </style>
</head>
<?php
if (isset($excel)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Occupancy.xls");
}
?>

<body>
    <div class="container">
        <br />
        <center>
            <h2>OCCUPANCY REPORT</h2>
            <h6><?php echo $tipe ?></h4>
                <h5>Per Tanggal: <?php echo $tanggalKirim ?></h5>
                <br>
        </center>
        <table border="1" class='table table-bordered' style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <?php
                    $tampung = '';
                    $x = '';
                    foreach ($data as $dt) {
                        $tanggal = $dt->tanggal;
                        if ($tampung != Carbon\Carbon::parse($tanggal)->day) {
                    ?>
                            <th>{{Carbon\Carbon::parse($tanggal)->day}}/{{Carbon\Carbon::parse($tanggal)->month}}</th>
                    <?php
                            $tampung = Carbon\Carbon::parse($tanggal)->day;
                        }
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php

                foreach ($kamar as $dtk) {
                ?>
                    <tr>
                        <td><b>[{{{App\Helpers::getRoomTypeFromRoomDetail($dtk->id_tipe)}}}]</b> {{$dtk->no_kamar}}</td>
                        <?php
                        $tampung = '';
                        foreach ($data as $dt) {
                            $tanggal = $dt->tanggal;
                            $day = Carbon\Carbon::parse($tanggal)->day;
                            $month = Carbon\Carbon::parse($tanggal)->month;
                            if ($tampung != Carbon\Carbon::parse($tanggal)->day) {
                                $cek = App\Helpers::getDateFromRoom($day, $month, $dtk->id_kamar);
                                if ($cek != 0) {
                                    $x = 'X';
                                } else {
                                    $x = '-';
                                }
                        ?>
                                <td>{{$x}}</td>
                        <?php
                                $tampung = Carbon\Carbon::parse($tanggal)->day;
                            }
                        }
                        ?>
                    </tr>

                    <tr>
                    </tr>
                <?php

                }
                ?>

                <tr>
                    <td><b> Jumlah Kamar Terisi </b></td>
                    <?php
                    $tanggal = array();
                    foreach ($data as $dt) {
                        array_push($tanggal, $dt->tanggal);
                    }

                    $dt_tanggal = array_unique($tanggal);

                    foreach ($dt_tanggal as $dt_tanggal) {
                        $day = Carbon\Carbon::parse($dt_tanggal)->day;
                        $month = Carbon\Carbon::parse($dt_tanggal)->month;
                    ?>

                        <td>[{{{App\Helpers::countRoomOccupied($month, $day, $roomType)}}}]</td>
                    <?php

                    }
                    ?>
                </tr>
            </tbody>
        </table>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>