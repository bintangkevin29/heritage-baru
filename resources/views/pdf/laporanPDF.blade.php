<!doctype html>
<html lang="en">
<?php

use App\Http\Controllers\InventarisController;
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
        body {
            font-size: 12px;
        }

        td {
            padding: 4px 10px !important;
        }
    </style>
</head>
<?php
if (isset($excel)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Inventaris.xls");
}
?>

<body>
    <div class="container">
        <br />
        <center>
            <h2>LAPORAN INVENTARIS</h2>
            <h5>Per Tanggal: <?php echo $tanggal->toFormattedDateString(); ?></h5>
            <h5>Golongan: <?php echo $golonganPrint ?></h5>
            <br>
        </center>
        <table border="1" class='table table-bordered' style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Golongan</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Jumlah Akhir</th>
                </tr>
            </thead>
            <tbody>
                @php $i=1 @endphp
                @foreach($data as $p)
                <tr>
                    <td style="width:20px">{{ $i++ }}</td>
                    <td style="width:20px">
                        {{$p->id_golongan}}.{{$p->id}}
                    </td>
                    <td>
                        <?php
                        echo InventarisController::getGolonganFromBarang($p->id_golongan);
                        ?>
                    </td>
                    <td>{{ $p->nama_barang }}</td>
                    <td>
                        {{$p->jumlah_awal}}
                        <?php
                        echo InventarisController::getSatuanFromBarang($p->id_satuan);
                        ?>
                    </td>
                    <td>
                        {{$p->jumlah}}
                        <?php
                        echo InventarisController::getSatuanFromBarang($p->id_satuan);
                        ?>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>