<!doctype html>
<html lang="en">
<?php

use App\Http\Controllers\InventarisController;
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
        body {
            font-size: 12px;
        }

        td {
            padding: 4px 10px !important;
        }
    </style>
</head>
<?php

try {
    foreach ($data as $dtdt) {
        $xxx = $dtdt->tanggal;
        $xxx = Carbon\Carbon::parse($xxx);
    }
    $bulan = $xxx->month;

    $tahun = $xxx->year;
} catch (Exception $e) {
    echo 'Tidak ada data untuk bulan terpilih';
    exit;
}

?>

<body>
    <div class="container">
        <br />
        <center>
            <h2>LAPORAN INVENTARIS</h2>
            <h5>
                Dicetak Tanggal: <?php echo Carbon\Carbon::parse($tanggal)->toFormattedDateString(); ?>
            </h5>
            <br>
            @if ($nm_golongan == "")
            <h5> {{$nm_golongan}} </h5>
            
            
            @else
            @foreach($nm_golongan as $nama)
            <h6> {{$nama->golongan}} </h6>
            @endforeach
            @endif
        </center>
        <br> 
        <div id="table-container">
            <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
            <table id="tabelqu" class="table table-striped datatable-responsive">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Keterangan</th>
                    <th>Nama Barang</th>
                    <th>Penanggungjawab</th>
                    <th>Jumlah Awal</th>
                    <th>Jumlah Akhir</th> 
                    <!-- {{$golongan}} -->
                </tr>
            </thead>
                    
            <!-- <ul style="list-style:none; padding-left:0px"> -->
            <tbody id="badan">
            <tr colspan="6" style="font-size:17px">
                <td colspan="6"><b>{{date("F", mktime(0, 0, 0, $bulan, 10))}} {{$tahun}}</b></td>
            </tr>
            <!-- </li> -->
            <!-- <ul style="list-style:none; font-size:17px; padding-left:10px"> -->
                <?php

                $printDate = '';
                $tampungDate = '';
                $tanda = '';
                foreach ($data as $dt) {
                    $date = Carbon\Carbon::parse($dt->tanggal);
                    $today = $date->format('l');
                    if ($tampungDate != $today) {
                ?>
                        <tr colspan="6" style="font-size: 14px">
                            <td colspan="6"><b>- {{$date->format('l')}}, {{$date->format('d')}}</b></td>
                        </tr>
                            <!-- <ul style="padding-left:10px"> -->
                                <?php
                                $dataDetail = InventarisController::getDetailKeluarMasuk($dt->tanggal, $golongan);
                                foreach ($dataDetail as $dtd) {
                                    $time = Carbon\Carbon::parse($dtd->tanggal);
                                    $time = $time->toTimeString();
                                    if ($dtd->keluarmasuk == 'keluar') {
                                        $tanda = '-';
                                    } else if ($dtd->keluarmasuk == 'masuk') {
                                        $tanda = '+';
                                    }
                                ?>
                                    <tr style="font-size: 13px; overflow:auto; list-style:none;border-bottom:1px solid black;padding:2.5px 5px">
                                        <div style="float:left">
                                        <b>
                                            <td>
                                                {{$time}} [{{strtoupper($dtd->keluarmasuk)}}] 
                                            </td>
                                            <td> [{{$tanda}}{{$dtd->jumlah}}] </td>    
                                        </b> 
                                        <td> {{$dtd->nama_barang}} </td>
                                        </div>
                                        <div style="float:right">
                                            <td> {{$dtd->pj}} </td>
                                            <td> {{$dtd->jumlahAwal}} {{$dtd->satuan}} </td> 
                                            <td> {{$dtd->jumlahAkhir}} {{$dtd->satuan}} </td>
                                        </div>
                                    </tr>
                                    <!-- </li> -->
                                <?php
                                }
                                ?>
                            <!-- </ul> -->
                        <!-- </li> -->
                <?php
                    }
                    $tampungDate = $today;
                }
                ?>
            <!-- </ul> -->
        <!-- </ul> -->
            </tbody>
            </table>
        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>