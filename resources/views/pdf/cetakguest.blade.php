@include('hotel.layouts.header')

<!doctype html>
<html lang="en">
<?php

use App\Http\Controllers\InventarisController;
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
        body {
            font-size: 15px;
        }

        td {
            padding: 4px 10px !important;
        }

        .col-6 {
            padding-bottom: 12px;
            float: left;
        }

        /* .no-bottom
        {
            padding-bottom: -10px !important;
        } */

        .col-6 span {
            font-size: 14px;
        }
    </style>
</head>
<?php
    $title = $dataGuest->title;
if($dataGuest->title == 1)
{
    $title = "Mr" ;
}

elseif($dataGuest->title == 2)
{
    $title = "Ms." ;
}

elseif($dataGuest->title == 3)
{
    $title = "Mrs." ;
}


$in = strtotime($data->check_in);
$out = strtotime($data->check_out);
$datediff = $out - $in;
$gap =  round($datediff / (60 * 60 * 24));
$harga_umum = number_format($data->harga_umum, 0, ',', '.');
$room_payment = number_format($data->harga_umum*2, 0, ',' ,'.') ;
?>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3><b>REGISTRATION</b></h3>
                <h6>Reservation id: {{$reservationID}}</h6>
                <h6>Reservation Date: {{$tanggalReservasi}}</h6>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-6">
                <b>Title</b>
                <br>
                <span>{{$title}}</span>
            </div>
            <div class="col-6">
                <b>Surname</b>
                <br>
                <span>{{$dataGuest->surname}}</span>
            </div>
            <div class="col-6">
                <b>Given Name</b>
                <br>
                <span>{{$dataGuest->first_name}}</span>
            </div>
            <div class="col-6">
                <b>Expected Check In</b>
                <br>
                <span>{{carbon\Carbon::parse($data->check_in)->toFormattedDateString()}}</span>
            </div>
            <div class="col-6">
                <b>Expected Check Out</b>
                <br>
                <span>{{carbon\Carbon::parse($data->check_out)->toFormattedDateString()}}</span>
            </div>
            
            <div class="col-6">
                <b>Room Number</b>
                <br>
                <span>{{$data->no_kamar}}</span>
            </div>

            <div class="col-6">
                <b>Room Type</b>
                <br>
                <span>{{$data->tipe}}</span>
            </div>
            
            <div class="col-6">
                <b>Deposite</b>
                <br>
                <span>{{$data->deposit}}</span>
            </div>
            
            <div class="col-6 no-bottom">
                <b> Payment Method </b> <br>
                <!-- <div class='col-6'> -->
                    <i class="icon-checkbox-unchecked"></i> American Express <br>
                    <i class="icon-checkbox-unchecked"></i> Euro/Master Card <br>
                    <i class="icon-checkbox-unchecked"></i> JCB <br>
                <!-- </div>
                <div class='col-6'> -->
                    <i class="icon-checkbox-unchecked"></i> Master Card <br>
                    <i class="icon-checkbox-unchecked"></i> Visa<br>
                    <i class="icon-checkbox-unchecked"></i> Cash
                <!-- </div> -->
            </div>

            
            <div class="col-6">
                <b>Price Room</b>
                <br>
                <span>{{$data->tipe}}</span> - 
                <span>Rp{{$harga_umum}}</span>
            </div>

            
            <div class="col-6">
                <b>Payment</b>
                <br>
                <span>Rp{{$room_payment}}</span> 
                <!-- <span>Rp{{$data->harga_umum}}</span> -->
            </div>

            <div class="col-6">
                <b>Duration</b>
                <br>
                <span>{{$gap}} Night(s)</span>
            </div>

        </div>

        <hr style='border: 1px solid black'>

        <div class="row">
            <div class="col-6">
            Bussiness Address <i class="icon-checkbox-unchecked"></i>
            </div>
            <div class="col-6">
                Private Address <i class="icon-checkbox-unchecked"></i><br>
            </div>
            <div class="col-6">
                <b>Address</b>
                <br>
                <span> {{$dataGuest->address}}</span>
            </div>

            <div class="col-6">
                <b>Postal Code</b>
                <br>
                <span> {{$dataGuest->postal_code}}</span>
            </div>
            
            <div class="col-6">
                <b>Country</b>
                <br>
                <span> {{$dataGuest->country}}</span>
            </div>

            <div class="col-6">
                <b>Telephone</b>
                <br>
                <span> {{$dataGuest->telephone}}</span>
            </div>
            <div class="col-6">
                <b>Email</b>
                <br>
                <span> {{$dataGuest->email}}</span>
            </div>
            <div class="col-6 ">
                <b>Date of Birth</b>
                <br>
                <span> {{$dataGuest->dob}}</span>
            </div>
            <div class="col-6">
                <b>Place of Birth</b>
                <br>
                <span> {{$dataGuest->pob}}</span>
            </div>

            <div class="col-6">
                <b>Nationality</b>
                <br>
                <span> {{$dataGuest->nationality}}</span>
            </div>
            <!-- <div class="col-6">
                <b>Company</b>
                <br>
                <span> {{$data->company ?? '-'}}</span>
            </div> -->

            <div class="col-6">
                <b>Issue Date</b>
                <br>
                <span>......................................</span>
            </div>

            <div class="col-6">
                <b>Place of Issue</b>
                <br>
                <span>......................................</span>
            </div>

            
            <div class="col-6">
                <b>Expiry Date</b>
                <br>
                <span>......................................</span>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
            Important: Money, jewels and other valuables are brought to the The Heritage Resort & Restaurant premises at the guest's sole risk. The Heritage Resort & Restaurant  and / or the management accept no liability and shall not be responsible for any loss or damage there to and guests remain solely responsible for the safekeeping of any such items. <br><br>

            Notwithstanding any method of payment, I agree that I am personally liable for all costs and charges incurred in the event that any such costs and charges are not paid in full and confirm that my responsibility and liability in that regard is not waived or released in any way. <br><br>

            I consent to the usage of my personal information for administrative and marketing purposes. The hotel guarantees not to disclose information to third parties other and affiliated companies.<br><br>

            </div>
        

        <hr style='border: 1px solid black'>
            <div class="col-12">
                By signing this form, I consent to the use of my personal information for the purpose described above. <br>
            </div>

            <div class="col-6 text-center">
                <b>Guest Signature</b>
                <br><br>
                <span>.................................................................</span>
            </div>

            
            <div class="col-6 text-center">
                <b>Date</b>
                <br><br>
                <span>.................................................................</span>
            </div>

            
            <div class="col-12 text-center">
                <b>Checked In By</b>
                <br><br>
                <span>.................................................................</span>
            </div>

            <div class="col-12 text-center">
                We respectfully remind you that check out time is 1pm noon
            </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>