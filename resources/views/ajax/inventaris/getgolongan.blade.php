    <div class="form-group col-md-12">
        <label>Golongan:</label>
        {{ csrf_field() }}
        <input style="display:none" autocomplete='off' type="text" value="{{$id}}" name="id" class="form-control">
        <input id="submit" type="text" value="{{$golongan}}" name="golongan" class="form-control" autofocus>
    </div>
    <div class="form-group col-md-12">
        <small><i>Tekan [ENTER] untuk mengubah.</i></small>
    </div>