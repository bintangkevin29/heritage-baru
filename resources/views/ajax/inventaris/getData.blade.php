<div class="form-group col-md-12">
    <label>Barang:</label>
    {{ csrf_field() }}
    <input style="display:none" autocomplete='off' type="text" value="{{$id}}" name="id" class="form-control">
    <input id="formSunting" type="text" value="{{$namaBarang}}" name="namaBarang" class="form-control" autofocus>
</div>

<div class="form-group col-md-12">
    <label for="exampleFormControlSelect1">Golongan</label>
    <select name="golongan" class=" form-control" id="golongan" data-live-search="true">
        <?php
        $isselected = '';
        foreach ($dataGolongan as $dts) {
            if ($id_golongan == $dts->id) {
                $isselected = 'selected';
            } else {
                $isselected = '';
            }
        ?>
            <option {{$isselected}} value="{{$dts->id}}">{{$dts->golongan}}</option>
        <?php
        }
        ?>
    </select>
</div>
<div class="form-group col-md-12">
    <label for="exampleFormControlSelect1">Satuan</label>
    <select name="satuan" class="form-control" id="satuan" data-live-search="true">
        <option selected disabled hidden>Pilih Satuan</option>
        <?php
        $isselected = '';
        foreach ($dataSatuan as $dts) {
            if ($id_satuan == $dts->id) {
                $isselected = 'selected';
            } else {
                $isselected = '';
            }
        ?>
            <option {{$isselected}} value="{{$dts->id}}">{{$dts->satuan}}</option>
        <?php
        }
        ?>
    </select>

</div>
<div class="form-group col-md-12">
    <label>Jumlah:</label>
    <input disabled id="formSunting" autocomplete="off" type="number" value="{{$jumlah}}" name="jumlah" class="form-control" autofocus>
</div>
<div class="form-group col-md-12">
    <small><i>Tekan [ENTER] untuk mengubah.</i></small>
</div>