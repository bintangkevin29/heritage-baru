<tr id="tr_{{$id}}" role="row" class="even">
    <td tabindex="0" class="sorting_1">{{$id}}</td>
    <td id="{{$id}}">{{$golongan}}</td>
    <td>
        <a data-toggle="modal" value="{{$id}}" onclick="bukaModal(getAttribute('value'))" class="btn btn-info"><i class="icon-pencil"></i> Sunting</a>
        <a value="{{$id}}" class="hapusGolongan btn btn-danger"><i class="icon-trash"></i> Hapus</a>
    </td>
</tr>