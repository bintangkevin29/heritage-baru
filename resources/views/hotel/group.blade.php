@include('hotel.layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">Group &mdash; List</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Group</span> &mdash; List</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('hotel.layouts.sidebar')

            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Group </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div style="padding-left:20px;">
                        <a href="Group" data-toggle="modal" data-target="#modal-add-group" class="btn btn-primary">Add New Group </a>
                    </div>

                    <div class="row" style="margin:0px; margin-top:10px">
                        <div class="col-xs-12">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success" role="alert">
                                {{$message}}
                            </div>
                            @endif
                        </div>
                    </div>

                    <table id="tabelqu" class="table table-striped datatable-responsive">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Details</th>
                                <th>Address</th>
                                <th>Members Count</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($data as $dt)
                            <tr>
                                <td>{{$dt->id}}</td>
                                <td>{{$dt->nama}}</td>
                                <td>{{$dt->keterangan}}</td>
                                <td>{{$dt->alamat}}</td>
                                <td>{{\App\Http\Controllers\GroupController::membersCount($dt->id)}}</td>
                                <td>
                                    <a href="{{url('group/members')}}/{{$dt->id}}" class="btn btn-success"><i class="icon-list"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#edit-modal-{{$dt->id}}" class="btn btn-info"><i class="icon-pencil"></i></a>
                                    <a onclick="return confirm('Delete this group ({{$dt->nama}})?')" href="{{url('group/delete')}}/{{ $dt->id }}" class="btn btn-danger"><i class="icon-trash"></i></a>
                                </td>
                            </tr>

                            <div class="modal fade" id="edit-modal-{{$dt->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Group</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{url('group/edit')}}">
                                            {{csrf_field()}}
                                            <div class="modal-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputEmail4">Name</label>
                                                        <input value="{{$dt->nama}}" autofocus type="text" name="name" class="form-control" id="inputEmail4" placeholder="Group Name">
                                                        <input value="{{$dt->id}}" autofocus type="text" name="id" class="form-control" style="display: none" id="inputEmail4" placeholder="Group Name">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputEmail4">Details</label>
                                                        <input value="{{$dt->keterangan}}" type="text" name="details" class="form-control" id="inputEmail4" placeholder="Group Name">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="inputEmail4">Address</label>
                                                        <input value="{{$dt->alamat}}" type="text" name="address" class="form-control" id="inputEmail4" placeholder="Group Name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /basic responsive configuration -->


                <!-- /whole row as a control -->

            </div>
        </div>
        <!-- /basic responsive configuration -->


        <!-- /whole row as a control -->

    </div>
    <!-- /main content -->


    <!-- Footer -->
    @include('layouts.footer')
    <!-- /footer -->
    <div class="modal fade" id="modal-add-group" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{url('group/add')}}">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Name</label>
                                <input autofocus type="text" name="name" class="form-control" id="inputEmail4" placeholder="Group Name">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Details</label>
                                <input type="text" name="details" class="form-control" id="inputEmail4" placeholder="Group Name">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputEmail4">Address</label>
                                <input type="text" name="address" class="form-control" id="inputEmail4" placeholder="Group Name">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>

</body>

</html>