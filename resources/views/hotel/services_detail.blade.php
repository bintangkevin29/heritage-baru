@include('hotel.layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->
    <?php
    foreach ($data as $key => $value) {
    }
    ?>

    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">Services &mdash; List</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Services</span> &mdash; List</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('hotel.layouts.sidebar')
            
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Services of <b><?=$value->surname?></b></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div style="padding-left:20px;">
                        <a data-toggle="modal" data-target="#modal_service" href="Services" class="btn btn-primary">Add New Services </a>
                    </div>
                    <table id="tabelqu" class="table table-striped datatable-responsive">
                        <thead>
                            <tr>
                                <th>ID</th> 
                                <th>Service</th> 
                                <th>Date</th> 
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <?php
                        $class = '';
                        $button = '';
                        foreach ($services as $key => $vservices) { 
                            $button = '<a onclick="return confirm(\'Are you sure?\')" href="Services_list/hapus/'.$value->id_reservasi.'" class="btn btn-danger"><i class="icon-cross"></i></a>   ';

                            ?>
                            <tr>
                                <td><?=$vservices->id_payment?></td>
                                <td><?=$vservices->desc_services?></td>
                                <td><?=Carbon\Carbon::parse($vservices->tanggal)->ToDayDateTimeString()?></td>
                                <td>IDR <?=number_format($vservices->service_price,2)?></td>
                                <td>
                                    <a data-toggle="modal" data-target="#modal<?=$vservices->id_payment?>" class="btn btn-info"><i class="icon-pencil"></i></a>   
                                    <a onclick="return confirm('Confirm deletion?');" href="{{url('hotel/services/delete')}}/<?=$vservices->id_payment?>" class="btn btn-danger"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                            <div id="modal<?=$vservices->id_payment?>" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header bg-primary">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                            <h6 class="modal-title">Edit Price</h6>
                                        </div>

                                        <div class="modal-body">
                                            <form method="post" action="{{url('hotel/services/edit')}}/<?=$vservices->id_payment?>">
                                                {{ csrf_field() }}

                                                <input style="display: none" value="<?=$vservices->id_payment?>" type="text" autocomplete="off" name="id_payment" class="form-control">
                                                 <input style="display: none" value="<?=$value->id_reservasi?>" type="text" autocomplete="off" name="id_reservasi" class="form-control">


                                                <div class="form-group">
                                                    <label>Price:</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">IDR</span>
                                                        <input required="" type="text" autocomplete="off" value="<?=$vservices->service_price?>" name="price" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                                    <input type="submit" name="tambah-service-reservasi" class="btn btn-primary" value="Submit">
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php

                        }
                        ?>
                        <tbody> 
                        </tbody>
                    </table>
                </div>
                <!-- /basic responsive configuration -->


                <!-- /whole row as a control -->

            </div>
        </div>
        <!-- /basic responsive configuration -->


        <!-- /whole row as a control -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->


<!-- Footer -->
@include('layouts.footer')
<!-- /footer -->
<div id="modal_service" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h6 class="modal-title">Tambah Item</h6>
            </div>

            <div class="modal-body">
                <form method="post" action="{{url('hotel/services/add')}}">
                    {{ csrf_field() }}

                    <input style="display: none" value="<?=$value->id_reservasi?>" type="text" autocomplete="off" name="id_reservasi" class="form-control">
                    
                    <div class="form-group">
                        <label>Service:</label> 
                        <?php
                        foreach ($basics as $key => $vbasics) {
                            ?>
                            <div class="radio">
                              <label>
                                <input required="" type="radio" name="services" value="<?=$vbasics->id_services?>"><?=$vbasics->desc_services?>
                            </label>
                        </div>  
                        <?php
                    }
                    ?>

                </div>

                <div class="form-group">
                    <label>Biaya:</label>
                    <div class="input-group">
                        <span class="input-group-addon">IDR</span>
                        <input required="" type="text" autocomplete="off" name="price" class="form-control">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                    <input type="submit" name="tambah-service-reservasi" class="btn btn-primary" value="Simpan">
                </div>

            </form>
        </div>
    </div>
</div>
</div>

<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script>
    $("#e1").select2({
        placeholder: "Leave this field empty if neccesary",
    });
    $('#e2').select2({
        minimumResultsForSearch: -1
    });

        // Jquery Dependency

        $("input[data-type='currency']").on({
            keyup: function() {
              formatCurrency($(this));
          },
          blur: function() { 
              formatCurrency($(this), "blur");
          }
      });


        function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");

  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
  }

    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

} else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
  }
}

  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}



</script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
<script type="text/javascript">

    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword){
            $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
            $("#submit").prop('disabled', true);

        }
        else{
            $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
            $("#submit"). removeAttr("disabled");
        }
    }

    $(document).ready(function () {
        $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>
<script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>

</body>
</html>