@include('hotel.layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">Reservation &mdash; Register</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Reservation</span> &mdash; Register</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('hotel.layouts.sidebar')
            
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Reservation &mdash; Register</h5> 
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div style="padding-left:20px;">
                        <a data-toggle="modal" href="{{url('user')}}" class="btn btn-sm btn-primary">Kembali</a>
                    </div> -->

                    <div class="panel-body">
                        <?php
                        if(Session::has('success'))
                        {
                            echo '<div class="alert alert-success">'. Session::get("success").'</div>';
                        }
                        ?>
                        <form class="form-horizontal" role="form" method="POST" action="{{url('hotel/reservation_room')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <legend class="col-md-12">Passport Scan</legend>
                                <div class="form-group col-md-6">
                                    <label>Passport Scan:</label>
                                    <input autocomplete='off' type="file" name="passport" class="form-control">
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="stepy-navigator">
                                        <input type="submit" value="Submit Reservation" class="button-next btn btn-primary">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- /basic responsive configuration -->


            <!-- /whole row as a control -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->


<!-- Footer -->
@include('layouts.footer')
<!-- /footer -->

<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script>
    $("#e1").select2({
        placeholder: "Leave this field empty if neccesary",
    });
    $('#e2').select2({
        minimumResultsForSearch: -1
    });

        // Jquery Dependency

        $("input[data-type='currency']").on({
            keyup: function() {
              formatCurrency($(this));
          },
          blur: function() { 
              formatCurrency($(this), "blur");
          }
      });


        function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
</script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
<script type="text/javascript">

    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword){
            $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
            $("#submit").prop('disabled', true);

        }
        else{
            $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
            $("#submit"). removeAttr("disabled");
        }
    }

    $(document).ready(function () {
        $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>
<script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>

</body>
</html>