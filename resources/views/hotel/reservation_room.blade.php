@include('hotel.layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">Reservation &mdash; Register</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Reservation</span> &mdash; Register</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('hotel.layouts.sidebar')
            
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Reservation &mdash; Register</h5> 
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div style="padding-left:20px;">
                        <a data-toggle="modal" href="{{url('user')}}" class="btn btn-sm btn-primary">Kembali</a>
                    </div> -->

                    <div class="panel-body">
                        <?php
                        if(Session::has('success'))
                        {
                            echo '<div class="alert alert-success">'. Session::get("success").'</div>';
                        }
                        ?>
                        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{url('hotel/reservation_room/save')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <legend class="col-md-12">Passport</legend>
                                <div class="form-group col-md-6">
                                    <label>Passport Scan:</label>
                                    <input required autocomplete='off' type="file" name="file" class="form-control">
                                </div>
                                <legend class="col-md-12">Room Data</legend>

                                <?php
                                $i = 1;
                                foreach ($data as $key => $value) {

                                    ?>
                                    <input style="display: none" autocomplete='off' type="text" value="<?=$value?>" name="f<?=$i;?>" placeholder="" class="form-control">

                                    <?php                           
                                    $i++;
                                }
                                ?>
                                <?php
                                foreach ($tipe as $key => $value) {
                                    ?>
                                    <div class="form-group col-md-3">
                                        <label><strong><?=$value['tipe']?></strong></label>
                                        <br>
                                        <br>
                                        <?php
                                        $disabled = '';
                                        foreach ($kamar as $key1 => $value1) {
                                            if($value1['id_tipe'] == $value['id_tipe'])
                                            {
                                                $disabled = '';
                                                foreach ($cek as $key => $vcek) {
                                                    if($value1->id_kamar == $vcek->id_kamar)
                                                    {
                                                        $disabled = 'disabled checked';
                                                    }
                                                     
                                                    // echo $vcek['id_kamar'];
                                                    // // echo ",";
                                                }
                                                ?>
                                                <div class="form-group col-md-12">
                                                    <label><?=$value1['no_kamar'];?> &nbsp;&nbsp;</label>
                                                    <div id="label-switch" class="make-switch switch-large" data-on-label="UNAVAILABLE" data-off-label="VACANT">
                                                        <input name="kamar[]" <?=$disabled?> type="checkbox" value="<?=$value1['id_kamar'];?>">
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            else
                                            {}

                                    }
                                    ?>


                                </div>
                                <?php
                            }
                            ?>

                            <div class="form-group col-md-12">
                                <div class="stepy-navigator">
                                    <a href="javascript:window.open('','_self').close();" class="button-next btn btn-default">Back</a>
                                    <input type="submit" value="Submit Reservation" class="button-next btn btn-primary">
                                </div>

                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <!-- /basic responsive configuration -->


        <!-- /whole row as a control -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->


<!-- Footer -->
@include('layouts.footer')
<!-- /footer -->

<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script>
    $("#e1").select2({
        placeholder: "Leave this field empty if neccesary",
    });
    $('#e2').select2({
        minimumResultsForSearch: -1
    });

        // Jquery Dependency

        $("input[data-type='currency']").on({
            keyup: function() {
              formatCurrency($(this));
          },
          blur: function() { 
              formatCurrency($(this), "blur");
          }
      });


        function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");

  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
  }

    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

} else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
  }
}

  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}



</script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
<script type="text/javascript">

    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword){
            $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
            $("#submit").prop('disabled', true);

        }
        else{
            $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
            $("#submit"). removeAttr("disabled");
        }
    }

    $(document).ready(function () {
        $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>
<script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>

</body>
</html>