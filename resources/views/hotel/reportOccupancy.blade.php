@include('hotel.layouts.header')

<body class="navbar-bottom">
    @include('layouts.navbar')

    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a>
                </li>
                <li class="active">Occupancy &mdash; Report</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>

        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Reservation</span> &mdash; List</h4>
            </div>
        </div>
    </div>

    <div class="page-container">
        <div class="page-content">
            @include('hotel.layouts.sidebar')

            <div class="content-wrapper">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Occupancy Report</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn-add">
                        <!-- MODAL INACTIVE -->
                        <!-- <a href="#" data-toggle="modal" id="tambah" class="btn btn-primary"><i class="icon-plus2"></i> &nbsp; Print Occupancy Report</a> -->

                        <!-- MODAL ACTIVE -->
                        <a href="#" data-toggle="modal" data-target="#modalLaporan" class="btn btn-primary"><i class="icon-plus2"></i> &nbsp; Buat Laporan Baru</a>
                    </div>

                    <div id="table-container">
                        <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>

                        <table id="tabelqu" class="table table-striped datatable-responsive">
                            <thead>
                                <tr>
                                    <th> Tipe Kamar</th>
                                    <?php
                                    $tampung = '';
                                    $x = '';
                                    foreach ($data as $dt) {
                                        $tanggal = $dt->tanggal;
                                        if ($tampung != Carbon\Carbon::parse($tanggal)->day) {
                                    ?>
                                            <th>{{Carbon\Carbon::parse($tanggal)->day}}/{{Carbon\Carbon::parse($tanggal)->month}}</th>
                                    <?php
                                            $tampung = Carbon\Carbon::parse($tanggal)->day;
                                        }
                                    }
                                    ?>
                                </tr>
                            </thead>

                            <tbody id="badan">
                                <tr>
                                    <?php foreach ($kamar as $dtk) { ?>
                                        <td>
                                            <b>[{{App\Helpers::getRoomTypeFromRoomDetail($dtk->id_tipe)}}]</b> {{$dtk->no_kamar}}
                                        </td>

                                        <?php
                                        $tampung = '';

                                        foreach ($data as $dt) {
                                            $tanggal = $dt->tanggal;
                                            $day = Carbon\Carbon::parse($tanggal)->day;
                                            $month = Carbon\Carbon::parse($tanggal)->month;

                                            if ($tampung != Carbon\Carbon::parse($tanggal)->day) {
                                                $cek = App\Helpers::getDateFromRoom($day, $month, $dtk->id_kamar);
                                                if ($cek != 0) {
                                                    $x = 'X';
                                                } else {
                                                    $x = '';
                                                }
                                        ?>

                                                <td> {{$x}} </td>

                                        <?php

                                                $tampung = Carbon\Carbon::parse($tanggal)->day;
                                            }
                                        }
                                        ?>
                                </tr>

                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    @include('layouts.footer');


    <div id="modalLaporan" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Cetak Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <form action="{{url('hotel/room/occupancyreport/cetak')}}" method="POST">
                    <div id="isi_modal" class="modal-body">
                        {{csrf_field()}}
                        <div class="form-group col-6">
                            <label>Pilih Bulan:</label>
                            <input id="formSunting" autocomplete="off" type="month" value="" name="bulan" class="form-control" autofocus>
                        </div>

                        <div class="form-check col-6">

                            <label>Room Type</label>
                            <select id="e2" name="roomType" data-placeholder="Select position" class="form-control select">
                                <option value="all"> Semua </option>
                                <?php foreach ($tipe_kamar as $tipe) { ?>
                                    <option value="<?= $tipe->id_tipe ?>">{{$tipe->tipe}}</option>
                                <?php } ?>
                            </select>
                            <br>
                        </div>
                        <div class="col-12 form-group checkbox">
                            <label for="excel"><input type="checkbox" name="excel" id="excel"> Format Excel </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group col-md-12">
                            <small><i>Gunakan browser Google Chrome apabila pilihan bulan tidak muncul.</i></small><br><br>
                            <input type="submit" value="Cetak" class="btn btn-primary">
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js" defer></script>
</body>

</html>