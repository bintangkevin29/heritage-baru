@include('hotel.layouts.header')

<body class="navbar-bottom">

@include('layouts.navbar')
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><i class="icon-home2 position-left"></i> Beranda</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Beranda</span> &mdash; Dasbor </h4>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistik</span></a>
                </div>
            </div>
        </div>
    </div>


    <div class="page-container">

        <div class="page-content">

            @include('hotel.layouts.sidebar')


            <div class="content-wrapper">

                <!-- <div class="row">

                    
                    <div class="col-lg-6">

                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphBdt" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div> 
    
                            </div>
  
                        </div>

                    </div>
                    
                    <div class="col-lg-6">

                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphPkh" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div> 
    
                            </div>
  
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphKks" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div> 
    
                            </div>
  
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphBpjs" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div> 
    
                            </div>
  
                        </div>

                    </div>


                    <div class="col-lg-6">

                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphPbi" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div> 
    
                            </div>
  
                        </div>

                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphUsulanPbi" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphBdtnonpkhkks" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphKksnonpkh" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphKkspkh" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphbdtbpjsnonkksnonpkhnonpbi" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphbpjsnonpbi" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphkksbpjsnonpkhnonpbi" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphpkhkksbpjsnonpbi" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphbdtpbinonkkspkh" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphusulanpbibpjs" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphkkspbi_nonpkh" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphpbibpjsbdtnonpkhkks" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphpbibpjsbdtkksnonpkh" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>

                    <div class="col-lg-6"> 
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <div id="container"> 
                                    <canvas id="graphpbibpjsbdtkkspkh" width="1013" height="506" class="chartjs-render-monitor"></canvas>
                                </div>  
                            </div> 
                        </div> 
                    </div>
                    
 
                </div> -->

 

            </div>

        </div>

    </div>


    @include('layouts.footer')

    <!-- <script> 
        var color = Chart.helpers.color;
         
        var barPbi = {
            labels: [<?php
            //  foreach ($dataPbi as $kecamatanPbi) {
            //      echo "'".$kecamatanPbi['kecamatan'].'\',';
            //  }
            //  ?>],
            // datasets: [{
            //  label: 'Jumlah Keluarga',
            //  backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            //  borderColor: window.chartColors.red,
            //  borderWidth: 1,
            //  data: [
            //      <?php
            //  foreach ($dataPbi as $jumlahPbi) {
            //      echo $jumlahPbi['jumlah'].',';
            //  }
                ?>
                ]
            }]

        };

        var barUsulanPbi = {
            labels: [<?php
                // foreach ($dataUsulanPbi as $kecamatanUsulanPbi) {
                //  echo "'".$kecamatanUsulanPbi['kecamatan'].'\',';
                // }
                ?>],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                    <?php
                // foreach ($dataUsulanPbi as $jumlahUsulanPbi) {
                //  echo $jumlahUsulanPbi['jumlah'].',';
                // }
                ?>
                ]
            }]

        };

        var barBdt = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                ]
            }]

        };

        var barPkh = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                ]
            }]

        };

        var barKks = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                ]
            }]

        };

        var barBpjs = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                ]
            }]

        };
        var barBdtnonpkhkks = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                ]
            }]

        };

        var barKksnonpkh = {
            labels: [<?php
                // foreach ($dataKksNonPkh as $kecamatanKksNonPkh) {
                //  echo "'".$kecamatanKksNonPkh['kec'].'\',';
                // }
                ?>],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                    <?php
                // foreach ($dataKksNonPkh as $jumlahKksNonPkh) {
                //  echo $jumlahKksNonPkh['jumlah'].',';
                // }
                ?>
                ]
            }]

        };

        var barKkspkh = {
            labels: [<?php
                // foreach ($dataKksPkh as $kecamatanKksPkh) {
                //  echo "'".$kecamatanKksPkh['kec'].'\',';
                // }
                ?>],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [
                    <?php
                // foreach ($dataKksPkh as $jumlahKksPkh) {
                //  echo $jumlahKksPkh['jumlah'].',';
                // }
                ?>
                ]
            }]
        };

        var barbdtbpjsnonkksnonpkhnonpbi = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barbpjsnonpbi = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barkksbpjsnonpkhnonpbi = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barpkhkksbpjsnonpbi = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barbdtpbinonkkspkh = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barkkspbi_nonpkh = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barusulanpbibpjs = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barpbibpjsbdtnonpkhkks = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barpbibpjsbdtkksnonpkh = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };

        var barpbibpjsbdtkkspkh = {
            labels: [],
            datasets: [{
                label: 'Jumlah Keluarga',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: []
            }]
        };





        window.onload = function() {

            var graphpbibpjsbdtkkspkh = document.getElementById('graphpbibpjsbdtkkspkh').getContext('2d');
            window.myBar = new Chart(graphpbibpjsbdtkkspkh, {
                type: 'bar',
                data: barpbibpjsbdtkkspkh,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin usulan penerima PBI/BPJS terdaftar BDT peserta KKS non PKH'
                    }
                }
            });

            var graphpbibpjsbdtkksnonpkh = document.getElementById('graphpbibpjsbdtkksnonpkh').getContext('2d');
            window.myBar = new Chart(graphpbibpjsbdtkksnonpkh, {
                type: 'bar',
                data: barpbibpjsbdtkksnonpkh,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin usulan penerima PBI/BPJS terdaftar BDT peserta KKS non PKH'
                    }
                }
            });



            var graphpbibpjsbdtnonpkhkks = document.getElementById('graphpbibpjsbdtnonpkhkks').getContext('2d');
            window.myBar = new Chart(graphpbibpjsbdtnonpkhkks, {
                type: 'bar',
                data: barpbibpjsbdtnonpkhkks,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin usulan penerima PBI/BPJS terdaftar BDT non PKH & KKS'
                    }
                }
            });

            var graphkkspbi_nonpkh = document.getElementById('graphkkspbi_nonpkh').getContext('2d');
            window.myBar = new Chart(graphkkspbi_nonpkh, {
                type: 'bar',
                data: barkkspbi_nonpkh,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Peserta KKS dan PBI non PKH'
                    }
                }
            });

            var graphusulanpbibpjs = document.getElementById('graphusulanpbibpjs').getContext('2d');
            window.myBar = new Chart(graphusulanpbibpjs, {
                type: 'bar',
                data: barusulanpbibpjs,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin usulan penerima PBI/BPJS'
                    }
                }
            });

            var graphbdtpbinonkkspkh = document.getElementById('graphbdtpbinonkkspkh').getContext('2d');
            window.myBar = new Chart(graphbdtpbinonkkspkh, {
                type: 'bar',
                data: barbdtpbinonkkspkh,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin terdaftar BDT & PBI non KKS & PKH'
                    }
                }
            });

            var graphpkhkksbpjsnonpbi = document.getElementById('graphpkhkksbpjsnonpbi').getContext('2d');
            window.myBar = new Chart(graphpkhkksbpjsnonpbi, {
                type: 'bar',
                data: barpkhkksbpjsnonpbi,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin terdaftar peserta PKH, KKS & BPJS non PBI'
                    }
                }
            });

            var graphkksbpjsnonpkhnonpbi = document.getElementById('graphkksbpjsnonpkhnonpbi').getContext('2d');
            window.myBar = new Chart(graphkksbpjsnonpkhnonpbi, {
                type: 'bar',
                data: barkksbpjsnonpkhnonpbi,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin terdaftar peserta KKS & BPJS non PKH non PBI'
                    }
                }
            });

            var graphbpjsnonpbi = document.getElementById('graphbpjsnonpbi').getContext('2d');
            window.myBar = new Chart(graphbpjsnonpbi, {
                type: 'bar',
                data: barbpjsnonpbi,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi keluarga miskin terdaftar BPJS non PBI'
                    }
                }
            });

            var graphbdtbpjsnonkksnonpkhnonpbi = document.getElementById('graphbdtbpjsnonkksnonpkhnonpbi').getContext('2d');
            window.myBar = new Chart(graphbdtbpjsnonkksnonpkhnonpbi, {
                type: 'bar',
                data: barbdtbpjsnonkksnonpkhnonpbi,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi keluarga miskin terdaftar BDT dan BPJS non KKS & non PKH & non PBI'
                    }
                }
            });


            var graphKkspkh = document.getElementById('graphKkspkh').getContext('2d');
            window.myBar = new Chart(graphKkspkh, {
                type: 'bar',
                data: barKkspkh,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi keluarga miskin terdaftar BDT peserta KKS & PKH'
                    }
                }
            });

            var graphKksnonpkh = document.getElementById('graphKksnonpkh').getContext('2d');
            window.myBar = new Chart(graphKksnonpkh, {
                type: 'bar',
                data: barKksnonpkh,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi keluarga miskin terdaftar BDT peserta KKS non PKH'
                    }
                }
            });

            var graphBdtnonpkhkks = document.getElementById('graphBdtnonpkhkks').getContext('2d');
            window.myBar = new Chart(graphBdtnonpkhkks, {
                type: 'bar',
                data: barBdtnonpkhkks,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Peserta KKS dan PKH'
                    }
                }
            });

            var graphPbi = document.getElementById('graphPbi').getContext('2d');
            window.myBar = new Chart(graphPbi, {
                type: 'bar',
                data: barPbi,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Terdaftar di PBI'
                    }
                }
            });

            var graphPkh = document.getElementById('graphPkh').getContext('2d');
            window.myBar = new Chart(graphPkh, {
                type: 'bar',
                data: barPkh,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Terdaftar di PKH'
                    }
                }
            });

            var graphUsulanPbi = document.getElementById('graphUsulanPbi').getContext('2d');
            window.myBar = new Chart(graphUsulanPbi, {
                type: 'bar',
                data: barUsulanPbi,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Terdaftar di Usulan PBI'
                    }
                }
            });

            var graphBdt = document.getElementById('graphBdt').getContext('2d');
            window.myBar = new Chart(graphBdt, {
                type: 'bar',
                data: barBdt,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Terdaftar di BDT'
                    }
                }
            });

            var graphKks = document.getElementById('graphKks').getContext('2d');
            window.myBar = new Chart(graphKks, {
                type: 'bar',
                data: barKks,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Terdaftar di KKS'
                    }
                }
            });

            var graphBpjs = document.getElementById('graphBpjs').getContext('2d');
            window.myBar = new Chart(graphBpjs, {
                type: 'bar',
                data: barBpjs,
                options: {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Rekapitulasi Keluarga Miskin Terdaftar di BPJS'
                    }
                }
            });




        };
  
    </script> -->
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/dashboard.js') }}"></script>

</body>
</html>
