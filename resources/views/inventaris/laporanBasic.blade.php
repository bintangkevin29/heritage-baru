@include('inventaris.layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">{{$modul}} &mdash; {{$title}}</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{$modul}}</span> &mdash; {{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('inventaris.layouts.sidebar')

            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{$title}} </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="btn-add">
                        <a href="#" data-target="#MyModal" data-toggle="modal" id="tambah" class="btn btn-primary"><i class="icon-plus2"></i> &nbsp; Buat Laporan Baru</a>
                    </div>
                    <!-- <div style="padding-left:20px;">
                        <a href="Services" class="btn btn-primary">Add New Services </a>
                    </div> -->
                    <div id="table-container">
                        <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
                        <table id="tabelqu" class="table table-striped datatable-responsive">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tanggal Laporan Dibuat</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody id="badan">
                                <?php
                                $class = '';
                                $button = '';
                                foreach ($data as $key => $value) {
                                    $button = '<a onclick="return confirm(\'Are you sure?\')" href="Services_list/hapus/' . $value->id . '" class="btn btn-danger"><i class="icon-cross"></i></a>   ';
                                ?>
                                    <tr id="tr_{{$value->id}}">
                                        <td><?= $value->id ?></td>
                                        <td><?= Carbon\Carbon::parse($value->tanggal)->toDayDateTimeString() ?></td>
                                        <td>
                                            <a href="{{url('inventaris/laporan/basic/download/')}}/{{$value->id}}" id="hapusdata" class="btn btn-success"><i class="icon-download"></i>&nbsp; Unduh Laporan</a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /basic responsive configuration -->


                <!-- /whole row as a control -->

            </div>
        </div>
        <!-- /basic responsive configuration -->


        <!-- /whole row as a control -->

    </div>
    <!-- /main content -->

    </div>
    <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Footer -->
    @include('layouts.footer')
    <!-- /footer -->

    <div id="MyModal" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{url('inventaris/laporan/basic/cetak')}}">

                    <div class="modal-header">
                        <h5 class="modal-title">Cetak Laporan</h5>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div id="isi_modal" class="modal-body">
                        {{csrf_field()}}
                        <div class="col-12 form-group">
                            <label for="">Pilih Golongan:</label>
                            <select name="golongan" id="" class="form-control">
                                <option value="all">Semua Golongan</option>
                                @foreach($dataGolongan as $dtg)
                                <option value="{{$dtg->id}}">{{$dtg->golongan}}</option>
                                @endforeach
                            </select>
                            </option>
                        </div>
                        <div class="col-12 form-group checkbox">
                            <label for="excel"><input type="checkbox" name="excel" id="excel"> Format Excel </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group col-md-12">
                            <input type="submit" value="Cetak" class="btn btn-primary form-control">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js" defer></script>

</body>

</html>