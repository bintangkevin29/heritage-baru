@include('inventaris.layouts.header')

<body id="tes" class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">{{$modul}} &mdash; {{$title}}</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{$modul}}</span> &mdash; {{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('inventaris.layouts.sidebar')

            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{$title}} </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="btn-add">
                        <button id="tambah" class="btn btn-primary"><i class="icon-plus2"></i> &nbsp; Tambah Barang</button>
                    </div>
                    <!-- <div style="padding-left:20px;">
                        <a href="Services" class="btn btn-primary">Add New Services </a>
                    </div> -->
                    <div id="table-container">

                    </div>
                </div>
                <!-- /basic responsive configuration -->


                <!-- /whole row as a control -->

            </div>
        </div>
        <!-- /basic responsive configuration -->


        <!-- /whole row as a control -->

    </div>
    <!-- /main content -->

    </div>
    <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Footer -->
    @include('layouts.footer')
    <!-- /footer -->

    <div id="MyModal" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Edit data</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div id="isi_modal" class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="form-group col-md-12">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalJumlah" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Konfirmasi Perubahan Jumlah Barang</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div id="modalJumlahIsi" class="modal-body">
                    <div id="jumlahConfirm" class="form-group col-md-12">

                    </div>
                    <div class="form-group col-md-12" id="penanggungjawabConfirm">

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group col-md-12">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalTambah" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Tambah Barang</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <form id="inventarisTambah" method="POST">
                    <div id="isi_modal_tambah" class="modal-body">
                        <div class="form-group col-md-12">
                            <label>Nama Barang:</label>
                            {{ csrf_field() }}
                            <input required autocomplete="off" type="text" value="" name="namaBarang" class="form-control" autofocus>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleFormControlSelect1">Golongan</label>
                            <select required name="golongan" class="selectpicker form-control" id="golongan" data-live-search="true">
                                <option value='' selected disabled hidden>Pilih Golongan</option>
                                <?php
                                foreach ($dataGolongan as $dts) {
                                ?>
                                    <option value="{{$dts->id}}">{{$dts->golongan}}</option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="exampleFormControlSelect1">Satuan</label>
                            <select required name="satuan" class="selectpicker form-control" id="satuan" data-live-search="true">
                                <option value='' selected disabled hidden>Pilih Satuan</option>
                                <?php
                                foreach ($dataSatuan as $dts) {
                                ?>
                                    <option value="{{$dts->id}}">{{$dts->satuan}}</option>
                                <?php
                                }
                                ?>
                            </select>

                        </div>
                        <div class="form-group col-md-12">
                            <label>Jumlah Awal:</label>
                            <input required autocomplete="off" type="number" value="" name="jumlah" class="form-control" autofocus>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="submit" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <div class="form-group col-md-12">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js" defer></script>
    <script type="text/javascript">
        function checkPasswordMatch() {
            var password = $("#txtNewPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();

            if (password != confirmPassword) {
                $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
                $("#submit").prop('disabled', true);

            } else {
                $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
                $("#submit").removeAttr("disabled");
            }
        }

        $(document).ready(function() {
            $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var isitable;

            var loaddataTable = function() {
                var table;
                $("#table-container").html('Mohon Menunggu...');

                $.ajax({
                    async: false,
                    url: "{{url('inventaris/table/data')}}",
                    context: document.body,
                    success: function(result) {
                        table = result;
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });

                return table;
            }();

            var jumlahId;
            var jumlah;

            $('#table-container').on('click', '#submitValue', function() {
                jumlahId = $(this).attr('value');
                var num = parseInt($('.jumlah_' + jumlahId).text());
                var oriNum = ($('.jumlah_' + jumlahId).attr('orinum'));
                $("#modalJumlah").modal();
                $("#jumlahConfirm").html('<label>Jumlah:</label><input value="' + num + '" id="formJumlah" autocomplete="off" type="number" name="jumlah" class="form-control" autofocus><input style="display:none" value="' + oriNum + '" id="formJumlah" autocomplete="off" type="number" name="orinum" class="form-control" autofocus><input style="display:none" value="' + jumlahId + '" id="formJumlah" autocomplete="off" type="number" name="id" class="form-control" autofocus>');
                $("#penanggungjawabConfirm").html('<label>Penanggungjawab:</label><input id="formJumlah" type="text" name="penanggungjawab" class="form-control" autofocus>');
            });


            //sunting jumlah barang
            $('#modalJumlah').on('keypress', "#formJumlah", function(e) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    $("#modalJumlah").modal('hide');
                    var jumlah = $("input[name=jumlah]").val();
                    var jumlahAwal = $("input[name=orinum]").val();
                    var id = $("input[name=id]").val();
                    var penanggungjawab = $("input[name=penanggungjawab]").val();

                    // alert(id);
                    $.ajax({
                        method: 'POST',
                        url: '{{url("inventaris/ajax/suntingjumlah")}}',
                        data: {
                            id: id,
                            jumlah: jumlah,
                            jumlahAwal: jumlahAwal,
                            penanggungjawab: penanggungjawab,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(result) { // What to do if we succeed
                            $("#jumlah_" + jumlahId).html(result);
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        }
                    });
                }
            });


            //sunting jumlah barang
            $('#table-container').on('click', '#submitJumlah', function() {
                $.ajax({
                    method: 'POST',
                    url: '{{url("inventaris/ajax/suntingjumlah")}}',
                    data: {
                        id: jumlahId,
                        jumlah: jumlah,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function(result) { // What to do if we succeed
                        $("#jumlah_" + jumlahId).html(result);
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });

            //buka modal sunting
            $('#table-container').on('click', '#bukaModal', function() {
                var str = $(this).attr('value');
                $("#MyModal").modal();
                $("#isi_modal").html("Memproses...");
                $.ajax({
                    url: "{{url('inventaris/ajax/getData')}}" + '/' + str,
                    success: function(result) {
                        $("#isi_modal").html(result);
                    }
                });
            });

            $('#table-container').html(loaddataTable);

            //sunting data
            $('#isi_modal').on('keypress', "#formSunting", function(e) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    $('#MyModal').modal('hide');
                    var id = $("input[name=id]").val();
                    var namaBarang = $("input[name=namaBarang]").val();
                    var jumlah = $("input[name=jumlah]").val();
                    var golongan = $("#golongan option:selected").val();
                    var satuan = $("#satuan option:selected").val();

                    if (namaBarang == '') {
                        alert('Harap mengisi semua field.');
                        exit;
                    }
                    if (jumlah == '') {
                        alert('Harap mengisi semua field.');
                        exit;
                    }
                    if (golongan == '') {
                        alert('Harap mengisi semua field.');
                        exit;
                    }
                    if (satuan == '') {
                        alert('Harap mengisi semua field.');
                        exit;
                    }
                    $('#modalTambah').modal('hide');

                    $.ajax({
                        method: 'POST',
                        url: '{{url("inventaris/ajax/suntingdata")}}',
                        data: {
                            id: id,
                            namaBarang: namaBarang,
                            jumlah: jumlah,
                            golongan: golongan,
                            satuan: satuan,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(result) { // What to do if we succeed
                            $("#tr_" + id).html(result);
                            window.location.href = window.location.href;
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        }
                    });
                }
            });
            $(document).on('submit', "#inventarisTambah", function(e) {
                e.preventDefault();
                var namaBarang = $("input[name=namaBarang]").val();
                var jumlah = $("input[name=jumlah]").val();
                var golongan = $("#golongan option:selected").val();
                var satuan = $("#satuan option:selected").val();

                if (namaBarang == '') {
                    alert('Harap mengisi semua field.');
                    exit;
                }
                if (jumlah == '') {
                    alert('Harap mengisi semua field.');
                    exit;
                }
                if (golongan == '') {
                    alert('Harap mengisi semua field.');
                    exit;
                }
                if (satuan == '') {
                    alert('Harap mengisi semua field.');
                    exit;
                }
                $('#modalTambah').modal('hide');

                $.ajax({
                    method: 'POST',
                    url: '{{url("inventaris/ajax/tambahdata")}}',
                    data: {
                        namaBarang: namaBarang,
                        jumlah: jumlah,
                        golongan: golongan,
                        satuan: satuan,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function(result) { // What to do if we succeed
                        alert('Berhasil menambah');
                        $('#table-container').load("{{url('inventaris/table/data')}}");
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        alert(jqXHR.responseText);

                    }
                });
            });

            $('#table-container').on('click', '#hapusdata', function() {
                var id = $(this).attr('value');
                var data = $("td[id=" + id + "]").text();
                if (confirm('Apakah anda yakin ingin menghapus data "' + data + '"?')) {
                    $.ajax({
                        url: "{{url('inventaris/ajax/hapusdata')}}" + "/" + id,
                        success: function(result) {
                            $('#tr_' + id).remove();
                            alert('Berhasil dihapus');
                        }
                    })
                } else {}
            });
            $('#tambah').click(function() {
                $("#modalTambah").modal();
            });
        });
    </script>
</body>

</html>