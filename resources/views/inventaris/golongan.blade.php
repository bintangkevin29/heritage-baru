@include('inventaris.layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">{{$modul}} &mdash; {{$title}}</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">{{$modul}}</span> &mdash; {{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('inventaris.layouts.sidebar')

            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic responsive configuration -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{$title}} </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="btn-add">
                        <button id="tambah" class="btn btn-primary"><i class="icon-plus2"></i> &nbsp; Tambah Golongan</button>
                    </div>
                    <!-- <div style="padding-left:20px;">
                        <a href="Services" class="btn btn-primary">Add New Services </a>
                    </div> -->
                    <div id="table-container">

                    </div>
                </div>
                <!-- /basic responsive configuration -->


                <!-- /whole row as a control -->

            </div>
        </div>
        <!-- /basic responsive configuration -->


        <!-- /whole row as a control -->

    </div>
    <!-- /main content -->

    </div>
    <!-- /page content -->

    </div>
    <!-- /page container -->


    <!-- Footer -->
    @include('layouts.footer')
    <!-- /footer -->

    <div id="MyModal" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Edit Golongan</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div id="isi_modal" class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="form-group col-md-12">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalTambah" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Tambah Golongan</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div id="isi_modal_tambah" class="modal-body">
                    <form id="golonganTambah" method="POST">
                        <div class="form-group col-md-12">
                            <label>Golongan:</label>
                            {{ csrf_field() }}
                            <input autocomplete="off" type="text" value="" name="golonganTambah" class="form-control" autofocus>
                        </div>
                        <div class="form-group col-md-12">
                            <input type="submit" class="btn btn-primary btn-block">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="form-group col-md-12">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
    <script type="text/javascript">
        function checkPasswordMatch() {
            var password = $("#txtNewPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();

            if (password != confirmPassword) {
                $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
                $("#submit").prop('disabled', true);

            } else {
                $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
                $("#submit").removeAttr("disabled");
            }
        }

        $(document).ready(function() {
            $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
        });
    </script>
    <script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var isitable;

            var loadGolonganTable = function() {
                var table;
                $("#table-container").html('Mohon Menunggu...');

                $.ajax({
                    async: false,
                    url: "{{url('inventaris/table/golongan')}}",
                    context: document.body,
                    success: function(result) {
                        table = result;
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });

                return table;
            }();


            $('#table-container').on('click', '#bukaModal', function() {
                var str = $(this).attr('value');


                $("#MyModal").modal();
                $("#isi_modal").html("Memproses...");
                $.ajax({
                    url: "{{url('inventaris/ajax/getgolongan')}}" + '/' + str,
                    success: function(result) {
                        $("#isi_modal").html(result);
                    }
                });
            });

            $('#table-container').html(loadGolonganTable);

            $('#isi_modal').on('keypress', "#submit", function(e) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    $('#MyModal').modal('hide');
                    var golongan = $("input[name=golongan]").val();
                    var id = $("input[name=id]").val();

                    $.ajax({
                        method: 'POST',
                        url: '{{url("inventaris/ajax/suntinggolongan")}}',
                        data: {
                            golongan: golongan,
                            id: id,
                            _token: '{{ csrf_token() }}'
                        },
                        success: function(result) { // What to do if we succeed
                            $("#" + id).html(result);
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        }
                    });
                }
            });

            $('#isi_modal_tambah').on('submit', "#golonganTambah", function(e) {
                e.preventDefault();
                $('#modalTambah').modal('hide');
                var golongan = $("input[name=golonganTambah]").val();

                $.ajax({
                    method: 'POST',
                    url: '{{url("inventaris/ajax/tambahgolongan")}}',
                    data: {
                        golongan: golongan,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function(result) { // What to do if we succeed
                        alert('Berhasil menambah');
                        $('#table-container').load("{{url('inventaris/table/golongan')}}");
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });

            $('#table-container').on('click', '#hapusGolongan', function() {
                var id = $(this).attr('value');
                var golongan = $("td[id=" + id + "]").text();
                if (confirm('Apakah anda yakin ingin menghapus golongan "' + golongan + '"?')) {
                    $.ajax({
                        url: "{{url('inventaris/ajax/hapusgolongan')}}" + "/" + id,
                        success: function(result) {
                            $('#tr_' + id).remove();
                            alert('Berhasil dihapus');
                        }
                    })
                } else {

                }
            });

            $('#tambah').click(function() {
                $("#modalTambah").modal();
            });
        });
    </script>


</body>

</html>