<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
<table id="tabelqu" class="table table-striped datatable-responsive">
    <thead>
        <tr>
            <th>ID</th>
            <th>Golongan</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody id="badan">
        <?php
        $class = '';
        $button = '';
        foreach ($data as $key => $value) {
            $button = '<a onclick="return confirm(\'Are you sure?\')" href="Services_list/hapus/' . $value->id . '" class="btn btn-danger"><i class="icon-cross"></i></a>   ';

        ?>
            <tr id="tr_{{$value->id}}">
                <td><?= $value->id ?></td>
                <td id="{{$value->id}}"><?= $value->golongan ?></td>
                <td>
                    <a data-toggle="modal" value="{{$value->id}}" id="bukaModal" class="btn btn-info"><i class="icon-pencil"></i> Sunting</a>
                    <a value="<?= $value->id ?>" id="hapusGolongan" class="btn btn-danger"><i class="icon-trash"></i> Hapus</a>
                </td>
            </tr>


        <?php
        }
        ?>
    </tbody>
</table>