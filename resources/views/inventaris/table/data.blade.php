<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
<table id="tabelqu" class="table table-striped datatable-responsive">
    <thead>
        <tr>
            <th>ID</th>
            <th>Kategori</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Satuan</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody id="badan">
        <?php
        $class = '';
        $button = '';
        foreach ($data as $key => $value) {
            $button = '<a onclick="return confirm(\'Are you sure?\')" href="Services_list/hapus/' . $value->id_data . '" class="btn btn-danger"><i class="icon-cross"></i></a>   ';

        ?>
            <tr id="tr_{{$value->id_data}}">
                <td><?= $value->id_golongan ?>.<?= $value->id_data ?></td>
                <td><?= $value->golongan ?></td>
                <td id="{{$value->id_data}}"><?= $value->nama_barang ?></td>
                <td id="jumlah_{{$value->id_data}}">
                   
                    <span orinum="{{$value->jumlah}}" class="jumlah_{{$value->id_data}}">
                        <?= $value->jumlah ?>
                    </span>
                    <a value="{{$value->id_data}}" class="btn" id="submitValue">
                        <i class="icon icon-pencil"></i>
                    </a>
                    <span id="showSubmit_{{$value->id_data}}">
                        
                    </span>
                    <span id="showCancel_{{$value->id_data}}">
                        
                    </span>
                </td>
                <td><?= $value->satuan ?></td>
                <td>
                    <a data-toggle="modal" value="{{$value->id_data}}" id="bukaModal" class="btn btn-info"><i class="icon-pencil"></i> Sunting</a>
                    <a value="<?= $value->id_data ?>" id="hapusdata" class="btn btn-danger"><i class="icon-trash"></i> Hapus</a>
                </td>
            </tr>


        <?php
        }
        ?>
    </tbody>
</table>