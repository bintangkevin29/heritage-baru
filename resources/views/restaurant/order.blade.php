@include('restaurant.layouts.header')

<body class="navbar-bottom">

    <!-- Main navbar -->
    @include('layouts.navbar')
    <!-- /main navbar -->


    <!-- Page header -->
    <div class="page-header">
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
                <li class="active">Check Out &mdash; List</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Bantuan</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Pengaturan
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Check Out</span> &mdash; List</h4>
            </div>
        </div>
    </div>
    <!-- /page header -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('restaurant.layouts.sidebar')
            
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">
                    <div class="row">

                        <div class="col-sm-7"> 


                            <!-- Bootstrap switch --> 
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title">List Pesanan</h5>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                            <li><a data-action="close"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="form-group pt-15">
                                        @if ( session()->has('message') )
                                        {!! session()->get('message') !!}
                                        @endif

                                        <form class="form" action="{{url('restaurant/order_save')}}" id="formnya" method="post">
                                            {{ csrf_field() }}

                                            <label class="text-semibold">Memesan sebagai:</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="order_as" value="1" onchange="tamu($(this).val())" class="">
                                                    Reguler
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input type="radio" id="eksekutor" name="order_as" value="2" onchange="penghuni($(this).val())" class="">
                                                    Penghuni
                                                </label>
                                            </div>  
                                            <div class="form-group ilangcoy" id="reservan">
                                                <label class="text-semibold">Nomor Kamar:</label>
                                                <select id="e1" name="room_num" data-placeholder="Select position" class="select"> 
                                                    <option id="defaultselect" value="0" style="display: none" selected="">--</option>

                                                    <?php
                                                    foreach ($kamar as $key => $vkamar) {
                                                        ?>
                                                        <option id="reservanselect" value="<?=$vkamar->id_reservasi_kamar?>"> <?=$vkamar->no_kamar?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group ilangcoy" id="nomorkamar">
                                                <label class="text-semibold">Nomor Meja:</label> 
                                                <input type="number" min="1" max="99" class="form-control" name="table_num">
                                            </div>
                                            <div class="form-group pt-15 ilangcoy" id="breakfast">
                                                <label class="text-semibold">Breakfast / Non Breakfast:</label>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" id="radiobreakfasta" name="is_breakfast" value="1" onchange="tes1($(this).val())" class="">
                                                        Breakfast
                                                    </label>
                                                </div>

                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" id="radiobreakfastb" name="is_breakfast" value="2" onchange="tes($(this).val())" class="">
                                                        Non Breakfast
                                                    </label>
                                                </div> 
                                            </div>

                                            <div class="form-group ilangcoy" id="daftarpesanan">
                                                <label class="text-semibold">Daftar Pesanan:</label>
                                                <div id="inidia">

                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <input id="submit" type="submit" class="ilangcoy btn btn-lg btn-block btn-primary" name="submit-order" value="Pesan">
                                        </form> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <!-- Basic setup -->
                            <div class="panel panel-flat ilangcoy" id="menu">
                                <div class="panel-heading">
                                    <h5 class="panel-title">Input Menu</h5>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                            <li><a data-action="close"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body" style="overflow:scroll;height:70vh;position: relative ;">
                                    <?php
                                    $tipecache = '';
                                    $i = 1;
                                    foreach ($menu as $key => $vmenu) {
                                        $tipe = $vmenu->id_menu_type;

                                        $buka = '';
                                        $tutup = '';
                                        if($vmenu->id_menu_type != $tipecache){
                                            $buka = '<div class="col-sm-12"><center><h6 class="panel-title">'.$vmenu->desc_type.'</h6><br></center></div>
                                            ';

                                        }
                                        echo $buka;
                                        ?>

                                        <div class="col-sm-4">
                                            <button style="font-size: 20px; height: 80px; margin-bottom:20px;" type="button" apa="<?=$vmenu->desc_menu?>" id="<?=$i?>" isi="R<?=$vmenu->id_menu?>" kode="<?=$vmenu->id_menu?>" class="btn-block btn-lg btn btn-success"><small style="font-size: 15px;">R<?=$vmenu->id_menu?></small><br><b><?=$vmenu->desc_menu?></b></button>
                                        </div>
                                        <script type="text/javascript"> 
                                            $("#<?=$i?>").click(function () {
                                                var x = $("#<?=$i?>").attr("apa");
                                                var y = $("#<?=$i?>").attr("isi");
                                                var z = $("#<?=$i?>").attr("kode");
                                                $("#inidia").append("<div class='col-md-3'><div class='checkbox'><label><input class='checkbesar' name='pesan[]' type='checkbox' checked value='"+z+"'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+y+" <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>"+x+"</b></label></div></div>");
                                            }); 

                                        </script>

                                        <?php
                                        $tipecache = $tipe;
                                        $i++;

                                    }
                                    ?>





                                    <script> 
                                        function penghuni(val){ 
                                            $("#breakfast").removeClass('ilangcoy');
                                            $("#reservan").removeClass('ilangcoy');  
                                            $("#nomorkamar").removeClass('ilangcoy');  
                                            $("#submit").addClass('ilangcoy'); 
                                            $("#daftarpesanan").addClass('ilangcoy'); 
                                            $("#menu").addClass('ilangcoy'); 
                                            $( "#radiobreakfasta" ).prop( "checked", false );
                                            $( "#radiobreakfastb" ).prop( "checked", false );


                                        }

                                        function tamu(val){ 
                                            $("#breakfast").addClass('ilangcoy');
                                            $("#nomorkamar").removeClass('ilangcoy'); 
                                            $("#reservan").addClass('ilangcoy'); 
                                            $("#menu").removeClass('ilangcoy'); 
                                            $("#daftarpesanan").removeClass('ilangcoy'); 
                                            $("#submit").removeClass('ilangcoy'); 
                                            $("#daftarpesanan").removeClass('ilangcoy'); 


                                        }

                                        function tes(val){ 
                                            $("#menu").removeClass('ilangcoy'); 
                                            $("#daftarpesanan").removeClass('ilangcoy'); 
                                            $("#submit").removeClass('ilangcoy'); 


                                        }

                                         function tes1(val){ 
                                            $("#menu").addClass('ilangcoy'); 
                                            $("#daftarpesanan").addClass('ilangcoy'); 
                                            $("#submit").removeClass('ilangcoy'); 


                                        }



                                    </script>

                                    <script type="text/javascript">
                                        document.getElementById('clear-button').addEventListener('click', function () {
                                            ["radiobreakfasta", "radiobreakfastb"].forEach(function(id) {
                                                document.getElementById(id).checked = false;
                                            });
                                            return false;
                                        })
                                    </script>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /wizard with callbacks -->


                    <!-- Footer -->

                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
        </div>
        <!-- /basic responsive configuration -->


        <!-- /whole row as a control -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->


<!-- Footer -->
@include('layouts.footer')
<!-- /footer -->


<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/extensions/responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script>
    $("#e1").select2({
        placeholder: "Leave this field empty if neccesary",
    });
    $('#e2').select2({
        minimumResultsForSearch: -1
    });

        // Jquery Dependency

        $("input[data-type='currency']").on({
            keyup: function() {
              formatCurrency($(this));
          },
          blur: function() { 
              formatCurrency($(this), "blur");
          }
      });


        function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");

  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
  }

    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

} else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
  }
}

  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}



</script>
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/datatables_responsive.js') }}"></script>
<script type="text/javascript">

    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword){
            $("#divCheckPasswordMatch").html("<font color='red'>Kata sandi belum cocok.</font>");
            $("#submit").prop('disabled', true);

        }
        else{
            $("#divCheckPasswordMatch").html("<font color='greend'>Kata sandi sudah cocok.</font>");
            $("#submit"). removeAttr("disabled");
        }
    }

    $(document).ready(function () {
        $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
    });

</script>
<script type="text/javascript" src="{{ asset('assets/js/switch/bootstrap-toggle.min.js') }}"></script>

</body>
</html>